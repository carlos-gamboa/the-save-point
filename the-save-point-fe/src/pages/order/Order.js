import React, { Component } from 'react';
import './ShoppingCart.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ShoppingCartItem from '../../components/card/shopping-cart-item/ShoppingCartItem';
import ProductHelper from '../../helpers/ProductHelper';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import ShoppingCartActions from '../../redux/shopping-cart/ShoppingCartActions';

class Order extends Component {

  constructor() {
    super();

    this.state = {
      orders: []
    };
  }

  componentDidMount() {
    const productsHelper = new ProductHelper();
    productsHelper.getShoppingCart()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setShoppingCart(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onRemoveFromShoppingCart = (id) => {
    const productsHelper = new ProductHelper();
    productsHelper.removeFromShoppingCart(id)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        this.props.onRemoveFromShoppingCart(id);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onUpdateProductQuantity = (id, quantity) => {
    const productsHelper = new ProductHelper();
    productsHelper.updateShoppingCart({quantity: quantity}, id)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        this.props.onUpdateProductQuantity(id, quantity);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { items } = this.props;

    return (
      <React.Fragment>
        <Header></Header>
        <div className='shopping-cart'>
          <h1 className='shopping-cart__heading'>My Shopping Cart</h1>
          <Link to='/checkout' className='shopping-cart__button'>Proceed to Checkout</Link>
          <div className='shopping-cart__wrapper'>
            { items.map((item) => {
              return (
                <ShoppingCartItem 
                  key={item.id}
                  item={item} 
                  onRemoveFromShoppingCart={this.onRemoveFromShoppingCart.bind(this)}
                  onUpdateProductQuantity={this.onUpdateProductQuantity.bind(this)}
                  match={this.props.match}
                />
              );
            })}
          </div>
        </div>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.ShoppingCartReducer.shoppingCart
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setShoppingCart: (items) => {
      dispatch({type: ShoppingCartActions.SET_SHOPPING_CART, items});
    },
    onRemoveFromShoppingCart: (itemId) => {
      dispatch({type: ShoppingCartActions.DELETE_FROM_CART, itemId});
    },
    onUpdateProductQuantity: (itemId, itemQuantity) => {
      dispatch({type: ShoppingCartActions.UPDATE_ITEM_QUANTITY, payload: {itemId: itemId, itemQuantity: itemQuantity}});
    }
  };
};

Order.propTypes = {
  items: PropTypes.array,
  setShoppingCart: PropTypes.func,
  onRemoveFromShoppingCart: PropTypes.func,
  onUpdateProductQuantity: PropTypes.func,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Order));
