import React, { Component } from 'react';
import './Account.scss';
import { withRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import logo from '../../assets/images/branding/logo.png';
import BillingContainer from '../../containers/billing-container/BillingContainer';
import BillingForm from '../../components/form/billing-form/BillingForm';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PaymentContainer from '../../containers/payment-container/PaymentContainer';
import PaymentForm from '../../components/form/payment-form/PaymentForm';
import OrderContainer from '../../containers/order-container/OrderContainer';
import AccountOverview from '../../components/layout/account-overview/AccountOverview';

class Account extends Component {
  constructor() {
    super();

    this.state = {
      tab: 'overview',
      showMenu: false
    };

  }

  handleToggleMobileMenu() {
    this.setState({
      showMenu: !this.state.showMenu
    });
  }

  handleTabChange = (event, tab) => {
    event.preventDefault();

    this.setState({
      tab: tab
    });
  }

  getTabClasses = (tab) =>{
    let classes = ['account__tab'];
    if (this.state.tab === tab) {
      classes.push('account__tab--active');
    }
    return classes.join(' ');
  }

  render() {
    const { showMenu } = this.state;
    const { firstName, lastName, match } = this.props;

    const navClasses = ['account__nav'];
    let mobileNavIcon = 'close';

    if (!showMenu) {
      navClasses.push('account__nav--hidden');
      mobileNavIcon = 'menu';
    }

    return (
      <main className='account'>
        <div className='account__wrapper'>
          <header className='account__header'>
            <Link to='/' className='account__branding'><img src={logo} alt='The Save Point Logo' className='account__logo'/></Link>
            <h1 className='account__heading'>My account</h1>
          </header>
          <div className='account__section'>
            <div className='account__info'>
              <div className='account__profile'>
                <div className='account__image-wrapper'>

                </div>
                <p className='account__greetings'>Hi, {firstName} {lastName}</p>
              </div>
              <div className='account__menu'>
                <div className="account__mobile">
                  <button className='account__mobile-toggle' onClick={(event) => {
                    this.handleToggleMobileMenu(event);
                  }} aria-expanded={showMenu} aria-label={mobileNavIcon === 'close' ? 'Close Menu' : 'Open Menu'}><i className='material-icons account__icon' aria-hidden='true'>{mobileNavIcon}</i></button>
                </div>
                <nav className={navClasses.join(' ')} aria-hidden={showMenu}>
                  <ul className='account__list'>
                    <li className='account__list-item'><NavLink exact={true} to={`${match.url}`} className='account__nav-button' activeClassName='account__nav-button--active'><i className='material-icons account__icon' aria-hidden='true'>home</i> <span className='account__tab-text'>Account Overview</span></NavLink></li>
                    {/* <li className='account__list-item' ><NavLink to={`${match.url}/edit`} className='account__nav-button' activeClassName='account__nav-button--active'><i className='material-icons account__icon'>person</i><span className='account__tab-text'>My Details</span></NavLink></li> */}
                    <li className='account__list-item'><NavLink to={`${match.url}/order`} className='account__nav-button' activeClassName='account__nav-button--active'><i className='material-icons account__icon' aria-hidden='true'>local_shipping</i><span className='account__tab-text'>My Orders</span></NavLink></li>
                    <li className='account__list-item'><NavLink to={`${match.url}/address`} className='account__nav-button' activeClassName='account__nav-button--active'><i className='material-icons account__icon' aria-hidden='true'>markunread_mailbox</i><span className='account__tab-text'>Address Book</span></NavLink></li>
                    {/* <li className='account__list-item'><NavLink to={`${match.url}/payment`} className='account__nav-button' activeClassName='account__nav-button--active'><i className='material-icons account__icon'>payment</i><span className='account__tab-text'>Payment Methods</span></NavLink></li> */}
                  </ul>
                </nav>
              </div>
            </div>
            <div className='account__content'>
              <Switch>
                <Route path={`${match.url}/address`} exact component={BillingContainer}></Route>
                <Route path={`${match.url}/address/:id/edit`}  render={() => (
                  <BillingForm edit={true}></BillingForm>
                )}></Route>
                <Route path={`${match.url}/address/new`} exact component={BillingForm}></Route>
                <Route path={`${match.url}/payment`} exact component={PaymentContainer}></Route>
                <Route path={`${match.url}/payment/:id/edit`}  render={() => (
                  <PaymentForm edit={true}></PaymentForm>
                )}></Route>
                <Route path={`${match.url}/payment/new`} exact component={PaymentForm}></Route>
                <Route path={`${match.url}/order`} exact component={OrderContainer}></Route>
                <Route component={AccountOverview}></Route>
              </Switch>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    firstName: state.AuthReducer.firstName,
    lastName: state.AuthReducer.lastName
  };
};

const mapDispatchToProps = () => {
  return {};
};

Account.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Account));
