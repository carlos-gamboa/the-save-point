import React, { Component } from 'react';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import { withRouter } from 'react-router-dom';
import ProductsContainer from '../../containers/products-container/ProductsContainer';

class ProductList extends Component {
  render() {
    return (
      <React.Fragment>
        <Header></Header>
        <ProductsContainer></ProductsContainer>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

export default withRouter(ProductList);