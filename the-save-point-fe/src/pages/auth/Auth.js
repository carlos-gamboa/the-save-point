import React, { Component } from 'react';
import logo from '../../assets/images/branding/logo.png';
import './Auth.scss';
import { withRouter, Link, NavLink, Switch, Route } from 'react-router-dom';
import Login from '../../components/form/login/Login';
import Register from '../../components/form/register/Register';
import PropTypes from 'prop-types';
const queryString =  require('query-string');

class Auth extends Component {

  componentDidUpdate = (prevProps) => {
    if (queryString.parse(this.props.location.search).section !== queryString.parse(prevProps.location.search).section) {
      this.setState({
        tab: queryString.parse(this.props.location.search).section
      });
    }
  }

  render = () => {
    const { match, location } = this.props;

    return (
      <main className='auth'>
        <div className='auth__branding'>
          <Link to='/' className='auth__branding-link'><img src={logo} alt='The Save Point Logo' className='auth__logo'/></Link>
        </div>
        <div className='auth__container'>
          <div className='auth__tabs'>
            <NavLink className='auth__tab' activeClassName='auth__tab--active' to={ {pathname: `${match.url}/register`, search: location.search}}>New to brand?</NavLink>
            <NavLink className='auth__tab' activeClassName='auth__tab--active' to={ {pathname: `${match.url}/login`, search: location.search}}>Already registered?</NavLink>
          </div>
          <div className='auth__content'>
            <Switch>
              <Route path={`${match.url}/login`} exact component={Login}></Route>
              <Route path={`${match.url}/register`} exact component={Register}></Route>
            </Switch>
          </div>
        </div>
      </main>
    );
  }
}

Auth.propTypes = {
  match: PropTypes.object,
  location: PropTypes.object,
  tab: PropTypes.string
};

export default withRouter(Auth);