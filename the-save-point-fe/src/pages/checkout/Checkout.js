import React, { Component } from 'react';
import './Checkout.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import CheckoutStepContainer from '../../containers/checkout-step-container/CheckoutStepContainer';
import CheckoutPayment from '../../components/layout/checkout-payment/CheckoutPayment';
import CheckoutComplete from '../../components/layout/checkout-complete/CheckoutComplete';
import ProductHelper from '../../helpers/ProductHelper';
import CheckoutActions from '../../redux/checkout/CheckoutActions';
import Loading from '../../components/loading/Loading';

class Checkout extends Component {

  constructor() {
    super();

    this.state = {
      loading: true
    };
  }

  componentDidMount = () => {
    const productsHelper = new ProductHelper();
    productsHelper.getShoppingCart()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setProducts(data.data);
        this.setState({
          loading: false
        });
      });
  }

  componentWillUnmount = () => {
    this.props.setStep(1);
  }

  handleContinueClick = () => {
    this.props.setStep(this.props.step + 1);
  }

  handleBackClick = () => {
    this.props.setStep(this.props.step - 1);
  }

  render() {
    const { loading } = this.state;
    const { items, step } = this.props;

    let stepContent = null;
    if (step === 1) {
      stepContent = <CheckoutStepContainer step={1} onContinueClicked={this.handleContinueClick.bind(this)} onBackClicked={this.handleBackClick.bind(this)} type='billing'></CheckoutStepContainer>;
    // } else if (step === 2) {
    //   stepContent = <CheckoutStepContainer step={2} onContinueClicked={this.handleContinueClick.bind(this)} onBackClicked={this.handleBackClick.bind(this)} type='payment'></CheckoutStepContainer>;
    } else if (step === 2) {
      stepContent = <CheckoutPayment onContinueClicked={this.handleContinueClick.bind(this)} onBackClicked={this.handleBackClick.bind(this)}></CheckoutPayment>;
    } else {
      stepContent = <CheckoutComplete></CheckoutComplete>;
    };

    let content;
    if (loading && items.length === 0) {
      content = <Loading></Loading>;
    }
    else {
      if (items.length === 0 && step === 1) {
        content = 
        <React.Fragment> 
          <p className='checkout__message'>Looks like your shopping cart is empty. You need to add items to your shopping cart before checkout</p>
          <Link to='/' className='checkout__button checkout__button--big'>Go back to shopping</Link>
        </React.Fragment>;
      } 
      else {
        content = <React.Fragment>
          <div className='checkout__step'>
            { stepContent }
          </div>
        </React.Fragment>;
      }
    }
      

    return (
      <React.Fragment>
        <Header></Header>
        <main className='checkout'>
          <h1 className='checkout__heading'>Checkout</h1>
          <div className='checkout__wrapper'>
            { content }
          </div>
        </main>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.ShoppingCartReducer.shoppingCart,
    billingId: state.CheckoutReducer.billingId,
    paymentId: state.CheckoutReducer.paymentId,
    products: state.CheckoutReducer.products,
    step: state.CheckoutReducer.step
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setProducts: (products) => {
      dispatch({type: CheckoutActions.SET_PRODUCTS, products});
    },
    setStep: (step) => {
      dispatch({type: CheckoutActions.SET_STEP, step});
    },
    setBilling: (billingId) => {
      dispatch({type: CheckoutActions.SET_BILLING, billingId});
    }
  };
};

Checkout.propTypes = {
  items: PropTypes.array,
  billingId: PropTypes.number,
  paymentId: PropTypes.number,
  products: PropTypes.array,
  setProducts: PropTypes.func,
  setBilling: PropTypes.func,
  setStep: PropTypes.func,
  step: PropTypes.number
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Checkout));