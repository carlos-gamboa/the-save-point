import React, { Component } from 'react';
import './WishList.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import WishListItem from '../../components/card/wish-list-item/WishListItem';
import ProductHelper from '../../helpers/ProductHelper';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import ShoppingCartActions from '../../redux/shopping-cart/ShoppingCartActions';
import WishListActions from '../../redux/wish-list/WishListActions';

class WishList extends Component {

  onRemoveFromWishList = (id) => {
    this.props.onRemoveFromWishList(id);
  }

  onAddToShoppingCart = (id) => {
    if (this.props.isLoggedIn) {
      const productsHelper = new ProductHelper();
      productsHelper.addToShoppingCart({quantity: 1, product: { id: id }})
        .then((response) => {
          if (response.status > 400) {
            throw response;
          }
          return response.json();
        })
        .then((data) => {
          this.props.addToShoppingCart(data.data);
          this.props.onRemoveFromWishList(id);
        });
    } 
    else {
      this.props.history.replace('/join/login');
    }
  };

  render() {
    const { items } = this.props;

    const emptyList = (items.length === 0) ? 
      <React.Fragment>
        <p className='wish-list__message'>Looks like your wish list is empty. Find some items that you like and add them to your wish list!</p>
        <Link to='/product' className='wish-list__button'>Go back to shopping</Link>
      </React.Fragment>
      :
      null;

    return (
      <React.Fragment>
        <Header></Header>
        <main className='wish-list'>
          <div className='wish-list__wrapper'>
            <h1 className='wish-list__heading'>My Wish List</h1>
            <React.Fragment>
              { emptyList }
              <div className='wish-list__item-wrapper'>
                { items.map((item) => {
                  return (
                    <WishListItem 
                      key={item.id}
                      item={item} 
                      onRemoveFromWishList={this.onRemoveFromWishList.bind(this)}
                      onAddToShoppingCart={this.onAddToShoppingCart.bind(this)}
                      match={this.props.match}
                    />
                  );
                })}
              </div>
            </React.Fragment>
          </div>
        </main>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.AuthReducer.isLoggedIn,
    items: state.WishListReducer.wishList
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToShoppingCart: (item) => {
      dispatch({type: ShoppingCartActions.ADD_TO_CART, item});
    },
    onRemoveFromWishList: (itemId) => {
      dispatch({type: WishListActions.DELETE_FROM_WISH, itemId});
    }
  };
};

WishList.propTypes = {
  isLoggedIn: PropTypes.bool,
  items: PropTypes.array,
  addToShoppingCart: PropTypes.func,
  onRemoveFromWishList: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WishList));
