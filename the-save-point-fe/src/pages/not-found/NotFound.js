import React from 'react';
import './NotFound.scss';
import { withRouter, Link } from 'react-router-dom';
import glitchIcon from '../../assets/images/icons/glitch.svg';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import ReactSVG from 'react-svg';

function NotFound() {
  return (
    <React.Fragment>
      <Header></Header>
      <div className='not-found'>
        <ReactSVG
          src={glitchIcon}
          svgClassName="not-found__svg"
          wrapper="span"
          className="not-found__svg-wrapper"
        />
        <h1 className='not-found__heading'>404 Not Found</h1>
        <div className='not-found__text-wrapper'>
          <p className='not-found__text'>Ooops! Looks like the page you&apos;re looking for doesn&apos;t exist.</p>
          <p className='not-found__text'>Maybe one of this was the page you were looking for?</p>
        </div>
        <nav className='not-found__nav'>
          <ul className='not-found__list'>
            <li className='not-found__list-item'>
              <Link to='/' className='not-found__link'>Home</Link>
            </li>
            <li className='not-found__list-item'>
              <Link to='/product' className='not-found__link'>Products</Link>
            </li>
          </ul>
        </nav>
      </div>
      <Footer></Footer>
    </React.Fragment>
  );
}

export default withRouter(NotFound);