import React, { Component } from 'react';
import './ShoppingCart.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ShoppingCartItem from '../../components/card/shopping-cart-item/ShoppingCartItem';
import ProductHelper from '../../helpers/ProductHelper';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import Loading from '../../components/loading/Loading';
import ShoppingCartActions from '../../redux/shopping-cart/ShoppingCartActions';

class ShoppingCart extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };
  }

  componentDidMount = () => {
    const productsHelper = new ProductHelper();
    productsHelper.getShoppingCart()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setShoppingCart(data.data);
        this.setState({
          loading: false
        });
      });
  }

  onRemoveFromShoppingCart = (id) => {
    const productsHelper = new ProductHelper();
    productsHelper.removeFromShoppingCart(id)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        this.props.onRemoveFromShoppingCart(id);
      });
  }

  onUpdateProductQuantity = (id, quantity) => {
    const productsHelper = new ProductHelper();
    productsHelper.updateShoppingCart({quantity: quantity}, id)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        this.props.onUpdateProductQuantity(id, quantity);
      });
  }

  render() {
    const { loading } = this.state;
    const { items } = this.props;

    const emptyCart = (items.length === 0) ? 
      <React.Fragment>
        <p className='shopping-cart__message'>Looks like your cart is empty. Find some items that you like and add them to your shopping cart!</p>
        <Link to='/' className='shopping-cart__button'>Go back to shopping</Link>
      </React.Fragment>
      :
      <Link to='/checkout' className='shopping-cart__button'>Proceed to Checkout</Link>;

    const content = (loading && items.length === 0) ? 
      <div className='shopping-cart__loading'><Loading></Loading></div>
      :
      <React.Fragment>
        { emptyCart }
        <div className='shopping-cart__item-wrapper'>
          { items.map((item) => {
            return (
              <ShoppingCartItem 
                key={item.id}
                item={item} 
                onRemoveFromShoppingCart={this.onRemoveFromShoppingCart.bind(this)}
                onUpdateProductQuantity={this.onUpdateProductQuantity.bind(this)}
                match={this.props.match}
              />
            );
          })}
        </div>
      </React.Fragment>
    ;

    return (
      <React.Fragment>
        <Header></Header>
        <main className='shopping-cart'>
          <div className='shopping-cart__wrapper'>
            <h1 className='shopping-cart__heading'>My Shopping Cart</h1>
            { content }
          </div>
        </main>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.ShoppingCartReducer.shoppingCart
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setShoppingCart: (items) => {
      dispatch({type: ShoppingCartActions.SET_SHOPPING_CART, items});
    },
    onRemoveFromShoppingCart: (itemId) => {
      dispatch({type: ShoppingCartActions.DELETE_FROM_CART, itemId});
    },
    onUpdateProductQuantity: (itemId, itemQuantity) => {
      dispatch({type: ShoppingCartActions.UPDATE_ITEM_QUANTITY, payload: {itemId: itemId, itemQuantity: itemQuantity}});
    }
  };
};

ShoppingCart.propTypes = {
  items: PropTypes.array,
  setShoppingCart: PropTypes.func,
  onRemoveFromShoppingCart: PropTypes.func,
  onUpdateProductQuantity: PropTypes.func,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShoppingCart));
