import React, { Component } from 'react';
import './Brand.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import Gallery from '../../containers/gallery/Gallery';
import ProductHelper from '../../helpers/ProductHelper';

class Brand extends Component {

  constructor(props) {
    super(props);

    this.state = {
      products: []
    };
  }

  componentDidMount() {
    this.getFeaturedProducts(this.props.brand);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.brand !== this.props.brand) {
      this.setState({
        products: []
      });
      this.getFeaturedProducts(this.props.brand);
    }
  }

  getFeaturedProducts = (brand) => {
    const productsHelper = new ProductHelper();
    productsHelper.getAllProducts(0, 'releaseDate,desc', '', '', brand)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          products: data.data
        });
      });
  }

  render() {
    const { products } = this.state;
    const { brand } = this.props;

    const gallery = (products.length === 0) ? null : <Gallery autoPlayInterval={2500} autoPlay={true} title='Featured Products' products={products}></Gallery>;

    return (
      <React.Fragment>
        <Header></Header>
        <main className='brand'>
          <h1 className='brand__title'>{brand}</h1>
          <section className='brand__card-container'>
            <Link to={`product?brand=${brand}&category=console`} className='brand__card'>
              <div className='brand__card-img' style={{
                backgroundImage: `url(https://s3.amazonaws.com/brainstation-project-3/products/${brand}-console.jpg)`
              }}></div>
              <h2 className='brand__card-text'>{brand} Consoles</h2>
            </Link>
            <Link to={`product?brand=${brand}&category=video_game`} className='brand__card' >
              <div className='brand__card-img' style={{
                backgroundImage: `url(https://s3.amazonaws.com/brainstation-project-3/products/${brand}-games.jpg)`
              }}></div>
              <h2 className='brand__card-text'>{brand} Games</h2>
            </Link>
            <Link to={`product?brand=${brand}&category=accessory`} className='brand__card' >
              <div className='brand__card-img' style={{
                backgroundImage: `url(https://s3.amazonaws.com/brainstation-project-3/products/${brand}-accessory.jpg)`
              }}></div>
              <h2 className='brand__card-text'>{brand} Accessories</h2>
            </Link>
          </section>
          <section className='brand__gallery'>
            { gallery }
          </section>
        </main>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

Brand.propTypes = {
  brand: PropTypes.string
};

export default withRouter(Brand);
