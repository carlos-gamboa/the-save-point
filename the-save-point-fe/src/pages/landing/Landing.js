import React, { Component } from 'react';
import './Landing.scss';
import logo from '../../assets/images/branding/logo-white.png';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import { withRouter, Link } from 'react-router-dom';
import ProductHelper from '../../helpers/ProductHelper';
import Gallery from '../../containers/gallery/Gallery';

class Landing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      featuredProducts: []
    };
  }

  componentDidMount() {
    const productsHelper = new ProductHelper();
    productsHelper.getAllProducts(0, 'releaseDate,desc', '', '', '')
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          featuredProducts: data.data
        });
      });
  }

  render() {
    const { featuredProducts } = this.state;

    const gallery = (featuredProducts.length === 0) ? null : <Gallery autoPlayInterval={2500} autoPlay={true} title='Featured Products' products={featuredProducts}></Gallery>;

    return (
      <React.Fragment>
        <Header></Header>
        <main className='landing'>
          <div className='landing__hero'>
            <div className='landing__hero-wrapper'>
              <div className='landing__text-wrapper'>
                <div className='landing__logo-wrapper'>
                  <img src={logo} alt='The Save Point Logo' className='landing__logo'/>
                </div>
                <h1 className='landing__heading-primary'>Every gaming product one click away from you</h1>
                <ul className='landing__categories'>
                  <li className='landing__category'>Consoles</li>
                  <li className='landing__category'>Video Games</li>
                  <li className='landing__category'>Accessories</li>
                </ul>
              </div>
              <Link to='/product' className='landing__button'>Start Shopping</Link>
            </div>
          </div>
          <div className='landing__gallery'>
            { gallery }
          </div>
        </main>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

export default withRouter(Landing);