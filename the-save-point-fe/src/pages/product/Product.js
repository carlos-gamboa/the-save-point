import React, { Component } from 'react';
import './Product.scss';
import ProductHelper from '../../helpers/ProductHelper';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactStars from 'react-stars';
import { connect } from 'react-redux';
import Gallery from '../../containers/gallery/Gallery';
import Header from '../../components/layout/header/Header';
import Footer from '../../components/layout/footer/Footer';
import Loading from '../../components/loading/Loading';
import ShoppingCartActions from '../../redux/shopping-cart/ShoppingCartActions';
import ReviewForm from '../../components/form/review-form/ReviewForm';
import ReviewItem from '../../components/card/review-item/ReviewItem';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {},
      loading: true,
      relatedProducts: []
    };
  }

  componentDidMount = () => {
    this.getProductInfo();
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.setState({
        loading: true
      });
      this.getProductInfo();
    }
  }

  getProductInfo = () => {
    const productsHelper = new ProductHelper();
    productsHelper.getProduct(this.props.match.params.category, this.props.match.params.id)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          product: data.data,
          loading: false
        });
        this.getRelatedProducts(data.data.id, data.data.brand, data.data.category);
      });
  }

  getRelatedProducts = (id, brand, category) => {
    const productsHelper = new ProductHelper();
    productsHelper.getAllProducts(0, 'releaseDate,desc', '', category, brand)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          relatedProducts: data.data.filter(product => product.id !== id)
        });
      });
  }

  handleAddShoppingCart = (event) => {
    event.preventDefault();
    if (this.props.isLoggedIn) {
      if (!this.props.isOnShoppingCart) {
        const productsHelper = new ProductHelper();
        productsHelper.addToShoppingCart({quantity: 1, product: this.state.product})
          .then((response) => {
            if (response.status > 400) {
              throw response;
            }
            return response.json();
          })
          .then((data) => {
            this.props.onAddToShoppingCart(data.data);
          });
      }
    } 
    else {
      this.props.history.replace(`/join/login?redirect=product+${this.state.product.category}+${this.state.product.id}`);
    }
  };

  getStockText = (leftInStock) => {
    let stock;
    if (leftInStock > 1) {
      stock = <React.Fragment>Only <span className='product__left-stock--bold'>{leftInStock}</span> left in stock</React.Fragment>;
    }
    else {
      stock = <React.Fragment>Out of stock</React.Fragment>;
    }
    return stock;
  }

  handleAddReview = (review) => {
    const reviews = [review, ...this.state.product.reviews];
    const product = {
      ...this.state.product,
      reviews
    };
    this.setState({
      product: product
    });
    this.getProductInfo();
  }

  render() {
    const { product, loading, relatedProducts } = this.state;
    const { isLoggedIn } = this.props;

    const gallery = (relatedProducts.length === 0) ? null : <Gallery title='Related Products' products={relatedProducts}></Gallery>;
    
    const reviewForm = <ReviewForm product={product} addReview={this.handleAddReview.bind(this)}></ReviewForm>;
    let reviews;
    if (!loading) {
      reviews = (product.reviews.length > 0) ? 
        <React.Fragment>
          <h2 className='product__review-title'>Reviews</h2>
          <div className='product__review-wrapper'>
            {
              product.reviews.map((review) => {
                return (
                  <ReviewItem 
                    key={review.id}
                    review={review}
                  />
                );
              })
            }
          </div>
        </React.Fragment>
        : null;
    } 


    let button;
    if (this.props.isOnShoppingCart) {
      button = <Link to='/my-cart' className='product__button'>Go to my Shopping Cart</Link>;
    } 
    else if (product.leftInStock === 0) {
      button = null;
    } else {
      button = <button className='product__button' onClick={(event) => this.handleAddShoppingCart(event)}><i className='material-icons product__icon'>add_shopping_cart</i>Add to Shopping Cart</button>;
    }

    return (
      <React.Fragment>
        <Header></Header>
        <main className='product'>
          {(loading) ?
            <Loading></Loading> 
            :
            <React.Fragment>
              <section className='product__breadcrumbs'>
                <Link to='/product' className='product__breadcrumb'>Product</Link>
                <span className='product__breadcrumb-separator'>&gt;</span>
                <Link to={`/product?brand=${product.brand}`} className='product__breadcrumb'>{product.brand}</Link>
                <span className='product__breadcrumb-separator'>&gt;</span>
                <Link to={`/product?brand=${product.brand}&category=${product.category}`} className='product__breadcrumb'>{product.category.replace('_', ' ')}</Link>
              </section>
              <section className='product__image-wrapper'>
                <img src={product.productPictureLink} alt='' className='product__image'/>
              </section>
              <section className='product__main'>
                <p className='product__name'>{product.name}</p>
                <div className='product__details'>
                  <ReactStars
                    className='product__stars'
                    count={5}
                    edit={false}
                    value={product.rating}
                    color1={'#737373'}
                    color2={'#D01345'} />
                  <p className='product__release'><span className='product__release--bold'>Release Date:</span> {product.releaseDate}</p>
                </div>
                <div className='product__pricing'>
                  <p className='product__price'>$ {product.price}</p>
                  <p className='product__left-stock'>{this.getStockText(product.leftInStock)}</p>
                </div>
                {button}
                <p className='product__description'>
                  {product.description}
                </p>
              </section>
              <section className='product__gallery'>
                { gallery }
              </section>
              <section className='product__reviews'>
                { isLoggedIn ? reviewForm : null }
                { reviews }
              </section>
            </React.Fragment>
          }
        </main>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: state.AuthReducer.isLoggedIn,
    isOnShoppingCart: state.ShoppingCartReducer.shoppingCart.find( item => item.product['id'] === Number(props.match.params.id) ) !== undefined
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToShoppingCart: (item) => {
      dispatch({type: ShoppingCartActions.ADD_TO_CART, item});
    }
  };
};

Product.propTypes = {
  isLoggedIn: PropTypes.bool,
  isOnShoppingCart: PropTypes.bool,
  onAddToShoppingCart: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Product));