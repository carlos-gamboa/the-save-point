import React, { Component } from 'react';
import './App.scss';
import { withRouter, Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Auth from './pages/auth/Auth';
import NotFound from './pages/not-found/NotFound';
import PrivateRoute from './components/private-route/PrivateRoute';
import Account from './pages/account/Account';
import Landing from './pages/landing/Landing';
import Product from './pages/product/Product';
import ShoppingCart from './pages/shopping-cart/ShoppingCart';
import Checkout from './pages/checkout/Checkout';
import ShoppingCartActions from './redux/shopping-cart/ShoppingCartActions';
import ProductHelper from './helpers/ProductHelper';
import ProductList from './pages/product-list/ProductList';
import Brand from './pages/brand/Brand';
import AuthActions from './redux/auth/AuthActions';
import WishListActions from './redux/wish-list/WishListActions';
import AuthHelper from './helpers/AuthHelper';
import WishList from './pages/wish-list/WishList';

class App extends Component {

  componentDidMount = () => {
    if (this.props.isLoggedIn) {
      const authHelper = new AuthHelper();
      authHelper.renew()
        .then((response) => {
          if (response.status !== 200) {
            throw response;
          }
          return response.json();
        })
        .then((data) => {
          const { firstName, lastName, userId, token } = data.data;
          this.props.onLogin(firstName, lastName, userId, token);
          this.props.getWishList(userId);
          this.getShoppingCart();
        })
        .catch(() => {
          this.props.onLogout();
        });
    } 
    else {
      this.props.setAnonWishList();
    }
  }

  getShoppingCart = () => {
    const productsHelper = new ProductHelper();
    productsHelper.getShoppingCart()
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setShoppingCart(data.data);
      });
  }

  getWishList = () => {

  }

  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path='/' component={Landing}></Route>
          <Route exact path={'/product'} component={ProductList}></Route>
          <Route exact path={'/wish-list'} component={WishList}></Route>
          <Route exact path='/nintendo' render={() => (
            <Brand brand='nintendo'></Brand>
          )} />
          <Route exact path='/playstation' render={() => (
            <Brand brand='playstation'></Brand>
          )} />
          <Route exact path='/xbox' render={() => (
            <Brand brand='xbox'></Brand>
          )} />
          <Route path={'/product/:category/:id'} component={Product}></Route>
          <Route path='/join' render={() => (
            <Auth tab='register'></Auth>
          )} />
          <PrivateRoute path='/my-cart' component={ShoppingCart}></PrivateRoute>
          <PrivateRoute path='/checkout' component={Checkout}></PrivateRoute>
          <PrivateRoute path='/account' component={Account}></PrivateRoute>
          <Route component={NotFound} />
        </Switch>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.AuthReducer.isLoggedIn
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWishList: (userId) => {
      dispatch({type: WishListActions.GET_WISH_LIST, userId});
    },
    setAnonWishList: () => {
      dispatch({type: WishListActions.SET_ANON});
    },
    setShoppingCart: (items) => {
      dispatch({type: ShoppingCartActions.SET_SHOPPING_CART, items});
    },
    onLogin: (firstName, lastName, userId, token) => {
      dispatch({type: AuthActions.LOGIN, payload: {firstName: firstName, lastName: lastName, userId: userId, token: token}});
    },
    onLogout: () => {
      dispatch({type: AuthActions.LOGOUT});
    }
  };
};

App.propTypes = {
  isLoggedIn: PropTypes.bool,
  getWishList: PropTypes.func,
  setShoppingCart: PropTypes.func,
  onLogin: PropTypes.func,
  onLogout: PropTypes.func,
  setAnonWishList: PropTypes.func
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
