import React from 'react';
import './Loading.scss';
import Spinner from '../../assets/images/spinner/grid.svg';

export default function Loading() {
  return (
    <div className='loading'>
      <img src={Spinner} alt='Spinner'/>
    </div>
  );
}
