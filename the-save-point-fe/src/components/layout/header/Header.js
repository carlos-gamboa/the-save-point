import React, { Component } from 'react';
import './Header.scss';
import logo from '../../../assets/images/branding/logo-white.png';
import { withRouter, NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AuthActions from '../../../redux/auth/AuthActions';
import ShoppingCartActions from '../../../redux/shopping-cart/ShoppingCartActions';
import WishListActions from '../../../redux/wish-list/WishListActions';

class Header extends Component {

  constructor() {
    super();
    this.state = {
      showMenu: false
    };
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.setState({
        showMenu: false
      });
    }
  }

  handleToggleMobileMenu() {
    this.setState({
      showMenu: !this.state.showMenu
    });
  }

  handleLogout(event) {
    event.preventDefault();
    this.props.onLogout();
    this.props.setAnonWishList();
    this.props.setShoppingCart([]);
  }

  render() {
    const { showMenu } = this.state;
    const { shoppingCartCount, wishListCount, isLoggedIn } = this.props;

    const navClasses = ['header__nav'];
    let mobileNavIcon = 'close';

    if (!showMenu) {
      navClasses.push('header__nav--hidden');
      mobileNavIcon = 'menu';
    }

    const authList = (isLoggedIn) ? 
      <div className='header__inner-nav'>
        <ul className='header__list'>
          <li className='header__list-item'><NavLink to='/my-cart' exact activeClassName='header__link--active' className='header__link'>
            <div className='header__shopping-cart'>
              <span className='header__shopping-count'>{shoppingCartCount}</span>
            </div>
            <i className='material-icons header__icon'>shopping_cart</i></NavLink>
          </li>
          <li className='header__list-item'><NavLink to='/wish-list' exact activeClassName='header__link--active' className='header__link'>
            <div className='header__shopping-cart'>
              <span className='header__shopping-count'>{wishListCount}</span>
            </div>
            <i className='fas fa-heart header__icon'></i></NavLink>
          </li>
          <li className='header__list-item'><NavLink to='/account' exact activeClassName='header__link--active' className='header__link'>My Account</NavLink></li>
          <li className='header__list-item'><button onClick={(event) => this.handleLogout(event)} className='header__link'>Logout</button></li>
        </ul>
      </div>
      :
      <div className='header__inner-nav'>
        <ul className='header__list'>
          <li className='header__list-item'><NavLink to='wish-list' exact activeClassName='header__link--active' className='header__link'>
            <div className='header__shopping-cart'>
              <span className='header__shopping-count'>{wishListCount}</span>
            </div>
            <i className='fas fa-heart header__icon'></i></NavLink>
          </li>
          <li className='header__list-item'><NavLink to='/join/login' exact activeClassName='header__link--active' className='header__link'>Login</NavLink></li>
          <li className='header__list-item'><NavLink to='/join/register' exact activeClassName='header__link--active' className='header__link'>Register</NavLink></li>
        </ul>
      </div>
    ;

    return (
      <header className='header'>
        <div className='header__wrapper'>
          <Link to='/' className='header__branding'>
            <img src={logo} alt='The Save Point Logo' className='header__logo'/>
          </Link>
          <div className="header__mobile">
            <button className='header__mobile-toggle' onClick={(event) => {
              this.handleToggleMobileMenu(event);
            }} aria-expanded={showMenu} aria-label={mobileNavIcon === 'close' ? 'Close Menu' : 'Open Menu'}><i className='material-icons header__icon' aria-hidden='true'>{mobileNavIcon}</i></button>
          </div>
          <nav className={navClasses.join(' ')} aria-hidden={showMenu}>
            <div className='header__inner-nav'>
              <ul className='header__list'>
                <li className='header__list-item'><NavLink to='/nintendo' exact activeClassName='header__link--active ' className='header__link header__link--uppercase'>Nintendo</NavLink></li>
                <li className='header__list-item'><NavLink to='/playstation' exact activeClassName='header__link--active' className='header__link header__link--uppercase'>PlayStation</NavLink></li>
                <li className='header__list-item'><NavLink to='/xbox' exact activeClassName='header__link--active' className='header__link header__link--uppercase'>Xbox</NavLink></li>
              </ul>
            </div>
            { authList }
          </nav>
        </div>
      </header>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.AuthReducer.isLoggedIn,
    shoppingCartCount: state.ShoppingCartReducer.shoppingCart.length,
    wishListCount: state.WishListReducer.wishList.length
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogout: () => {
      dispatch({type: AuthActions.LOGOUT});
    },
    setAnonWishList: () => {
      dispatch({type: WishListActions.SET_ANON});
    },
    setShoppingCart: (items) => {
      dispatch({type: ShoppingCartActions.SET_SHOPPING_CART, items: items});
    }
  };
};

Header.propTypes = {
  isLoggedIn: PropTypes.bool,
  shoppingCartCount: PropTypes.number,
  wishListCount: PropTypes.number,
  history: PropTypes.object,
  onLogout: PropTypes.func,
  setAnonWishList: PropTypes.func,
  setShoppingCart: PropTypes.func,
  location: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
