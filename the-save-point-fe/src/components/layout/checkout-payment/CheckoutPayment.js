import React, { Component } from 'react';
import './CheckoutPayment.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ProductHelper from '../../../helpers/ProductHelper';
import CheckoutActions from '../../../redux/checkout/CheckoutActions';
import ShoppingCartActions from '../../../redux/shopping-cart/ShoppingCartActions';
import PaypalButton from '../../buttons/paypal-button/PaypalButton';
import Environment from '../../../env';
import Loading from '../../loading/Loading';

class CheckoutPayment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      message: '',
      showMessage: false,
      messageClass: []
    };

    this.submitted = false;
  }

  componentDidMount = () => {
    const productHelper = new ProductHelper();
    productHelper.checkout(this.props.products, this.props.billingId)
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setOrder(data.data);
        this.setState({
          loading: false
        });
      })
      .catch((error) => {
        error.json()
          .then((errorDecoded) => {
            this.setState({
              loading: false,
              message: errorDecoded.message,
              showMessage: true,
              messageClass: ['checkout-payment__message', 'checkout-payment__message--error']
            });
          });
      });
  }

  handleBackClick = (event) => {
    event.preventDefault();
    this.props.onBackClicked();
  }

  handlePaypalSuccess = () => {
    this.props.order.status = 'accepted';
    this.setState({
      message: 'The transaction was a success!',
      showMessage: true,
      messageClass: ['checkout-payment__message', 'checkout-payment__message--success']
    });
    this.setMessageTimeout(5000, false);
    const productHelper = new ProductHelper();
    productHelper.updateOrder(this.props.order, this.props.order.id)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setOrder(data.data);
        this.props.emptyShoppingCart();
        this.props.onContinueClicked();
      });
  }

  handleCustomPaypalError = (error) => {
    this.props.setOrder(null);
    this.setState({
      message: error,
      showMessage: true,
      messageClass: ['checkout-payment__message', 'checkout-payment__message--error']
    });
  }

  handlePaypalError = () => {
    this.setState({
      message: 'Ooops! An error ocurred during the transaction. Please try again!',
      showMessage: true,
      messageClass: ['checkout-payment__message', 'checkout-payment__message--error']
    });
    this.setMessageTimeout(5000, false);
  }

  setMessageTimeout = (time) => {
    setTimeout(() => {
      this.setState({
        message: '',
        showMessage: false,
        messageClass: []
      });
    }, time);
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['checkout-payment__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('checkout-payment__input--invalid');
    }
    return classes.join(' ');
  }

  getTotalPrice = () => {
    let totalPrice = 0;
    for (const product of this.props.products) {
      totalPrice += (product.quantity * product.product.price);
    }
    return totalPrice;
  }

  render() {
    const { message, showMessage, messageClass, loading } = this.state;
    const { products, order } = this.props;

    const displayMessage = (showMessage) 
      ?
      <div className={messageClass.join(' ')}>
        <p className='checkout-payment__message-text'>{ message }</p>
      </div>
      : null;

    const content = (order === null) ?
      <React.Fragment>
        {displayMessage}
        <div className='checkout-payment__actions'>
          <Link to='/my-cart' className='checkout-payment__button checkout-payment__button--big'>Edit My Shopping Cart</Link>
        </div>
      </React.Fragment>
      :
      <React.Fragment>
        <div className='checkout-payment__products'>
          <p className='checkout-payment__product-title'>Product</p>
          <p className='checkout-payment__product-title checkout-payment__product-title--desktop'>Price</p>
          <p className='checkout-payment__product-title checkout-payment__product-title--desktop'>Quantity</p>
          <p className='checkout-payment__product-title'>Total</p>
          {
            products.map(product => {
              return (
                <React.Fragment key={product.product.id}>
                  <div className='checkout-payment__product-description'>
                    <div className='checkout-payment__product-image-wrapper'>
                      <img src={product.product.productPictureLink} alt={product.product.name} className='checkout-payment__product-image'/>
                    </div>
                    <p className='checkout-payment__product-info checkout-payment__product-name'>{product.product.name}</p>
                  </div>
                  <div className='checkout-payment__product-description checkout-payment__product-description--desktop'>
                    <p className='checkout-payment__product-info checkout-payment__product-info--center'>${product.product.price}</p>
                  </div>
                  <div className='checkout-payment__product-description checkout-payment__product-description--desktop'>
                    <p className='checkout-payment__product-info checkout-payment__product-info--center'>{product.quantity}</p>
                  </div>
                  <div className='checkout-payment__product-description'>
                    <p className='checkout-payment__product-info checkout-payment__product-info--center'>${(product.quantity * product.product.price).toFixed(2)}</p>
                  </div>
                </React.Fragment>
              );
            })
          }
          {/* <p className='checkout-payment__product-total'>Total</p>
          <p className='checkout-payment__product-price'>${this.getTotalPrice().toFixed(2)}</p> */}
        </div>
        <div className='checkout-payment__total'>
          <div className='checkout-payment__total-wrapper'>
            <p className='checkout-payment__product-title checkout-payment__product-title--rigth'>Total</p>
            <p className='checkout-payment__product-info'>${this.getTotalPrice().toFixed(2)}</p>
          </div>
        </div>
        <div className='checkout-payment__paypal-wrapper'>
          <div className='checkout-payment__paypal'>
            <PaypalButton
              total={this.getTotalPrice()}
              currency='USD'
              env={Environment.PAYPAL_ENV}
              orderId={order.id}
              commit={true}
              client={{
                sandbox: Environment.PAYPAL_CLIENT,
                production: Environment.PAYPAL_PROD
              }}
              onSuccess={this.handlePaypalSuccess.bind(this)}
              onError={this.handlePaypalError.bind(this)}
              onOrderExpired={this.handleCustomPaypalError.bind(this)}
            ></PaypalButton>
          </div>
        </div>
        <div className='checkout-payment__actions'>
          <button className='checkout-payment__button' onClick={(event) => this.handleBackClick(event)}>Back</button>
        </div>
        {displayMessage}
      </React.Fragment>;

    const displayContent = (loading) ? <div className='checkout-payment__loading'><Loading></Loading></div>
      :
      <React.Fragment>{content}</React.Fragment>;

    return (
      <div className='checkout-payment'>
        <h1 className='checkout-payment__heading'>Place your order</h1>
        {displayContent}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.CheckoutReducer.products,
    billingId: state.CheckoutReducer.billingId,
    paymentId: state.CheckoutReducer.paymentId,
    order: state.CheckoutReducer.order
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setOrder: (order) => {
      dispatch({type: CheckoutActions.SET_ORDER, order});
    },
    emptyShoppingCart: () => {
      dispatch({type: ShoppingCartActions.CHECKOUT});
    }
  };
};

CheckoutPayment.propTypes = {
  products: PropTypes.array,
  billingId: PropTypes.number,
  paymentId: PropTypes.number,
  onContinueClicked: PropTypes.func,
  onBackClicked: PropTypes.func,
  setOrder: PropTypes.func,
  emptyShoppingCart: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object,
  order: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckoutPayment));
