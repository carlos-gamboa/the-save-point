import React from 'react';
import './CheckoutComplete.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

function CheckoutComplete(props) {
  return (
    <div className='checkout-complete'>
      <h2 className='checkout-complete__heading'>Your order has been placed</h2>
      <div className='checkout-complete__order'>
        <p className='checkout-complete__order-title'>Products</p>
        {
          props.order.shoppingCart.map(item => {
            return (
              <React.Fragment key={item.product.id}>
                <p className='checkout-complete__product-info'>{item.quantity} &mdash; {item.product.name}</p>
              </React.Fragment>
            );
          })
        }
      </div>
      <p className='checkout-complete__arriving'>It&apos;s set to arrive <span className='checkout-complete__arriving--bold'>{props.order.arrivingDate}</span></p>
      <Link to='/account/order' className='checkout-complete__button'>Check my orders</Link>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    order: state.CheckoutReducer.order
  };
};

const mapDispatchToProps = () => {
  return {
  };
};

CheckoutComplete.propTypes = {
  order: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckoutComplete));
