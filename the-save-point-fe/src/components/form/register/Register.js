import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './Register.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import FormValidator from '../../../helpers/FormValidator';
import AuthHelper from '../../../helpers/AuthHelper';
import User from '../../../models/User';

class Register extends Component {
  constructor() {
    super();

    this.validator = new FormValidator([
      { 
        field: 'username', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Email is required.' 
      },
      { 
        field: 'username',
        method: 'isEmail', 
        validWhen: true, 
        message: 'That is not a valid email.'
      },
      { 
        field: 'firstName', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'First Name is required.' 
      },
      { 
        field: 'lastName', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Last Name is required.' 
      },
      { 
        field: 'password', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Password is required.'
      },
      {
        field: 'password', 
        method: 'matches',
        args: [/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/],
        validWhen: true, 
        message: 'Password must be at least 8 characters long and contain at least 1 letter and 1 number.'
      },
      { 
        field: 'dateOfBirth', 
        method: this.isOldEnough, 
        validWhen: true, 
        message: 'You must be at least 15 years old.'
      }
    ]);

    this.state = {
      username: '',
      firstName: '',
      lastName: '',
      password: '',
      dateOfBirth: new Date(),
      gender: 'male',
      validation: this.validator.isValid(),
      message: '',
      showMessage: false,
      messageClass: []
    };

    this.submitted = false;
  }

  isOldEnough = (date) => {
    const dateOfBirth = new Date(date);
    const today = new Date();
    let age = today.getFullYear() - dateOfBirth.getFullYear();
    if (today.getMonth() < dateOfBirth.getMonth() || (today.getMonth() === dateOfBirth.getMonth() && today.getDate() < dateOfBirth.getDate())) {
      age--;
    }
    return age >= 15; 
  }

  handleInputChange = (event) => {
    event.preventDefault();

    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleDateChange = (date) => {
    this.setState({ dateOfBirth: date });
  }

  handleGenderChange = (event) => {
    this.setState({
      gender: event.target.value
    });
  }
    
  handleFormSubmit = (event) => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      this.register();
    }
  }

  register= () => {
    const authHelper = new AuthHelper();
    authHelper.register(new User(this.state))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then(() => {
        this.setState({
          message: 'You\'ve successfully joined The Save Point.',
          showMessage: true,
          messageClass: ['register__message', 'register__message--success']
        });
        this.setMessageTimeout(3000, true);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['register__message', 'register__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  setMessageTimeout = (time, redirect) => {
    setTimeout(() => {
      this.setState({
        message: '',
        showMessage: false,
        messageClass: []
      });
      if (redirect) {
        this.props.history.push('login' + this.props.location.search);
      }
    }, time);
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['register__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('register__input--invalid');
    }
    return classes.join(' ');
  }

  getValidationError = (validation, fieldToValidate) => {
    const validationError = (validation[fieldToValidate].isInvalid) ?
      <span className='register__validation-error'>{validation[fieldToValidate].message}</span>
      : null;
    
    return validationError;
  }

  render() {

    const { username, firstName, lastName, password, dateOfBirth, gender, message, showMessage, messageClass } = this.state;
    const displayMessage = (showMessage) 
      ?
      <div className={messageClass.join(' ')}>
        <p className='register__message-text'>{ message }</p>
      </div>
      : null;

    // If the form has been submitted at least once.
    let validation = this.submitted ?                         
      this.validator.validate(this.state) : // Check validity every time we render.
      this.state.validation; // Otherwise just use what's in state

    return (
      <section className='register'>
        <div className='register__container'>

          <h1 className='register__heading'>Sign up using your email and password</h1>

          <form className='register__form'>

            <div className='register__form-control'>
              <label htmlFor='username' className='register__label'>Email address</label>
              <input type='email' id='username' className={this.getFormInputClasses(validation, 'username')}
                name='username'
                value={username}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'username')}
            </div>

            <div className='register__form-control'>
              <label htmlFor='firstName' className='register__label'>First name</label>
              <input type='text' id='firstName' className={this.getFormInputClasses(validation, 'firstName')}
                name='firstName'
                value={firstName}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'firstName')}
            </div>

            <div className='register__form-control'>
              <label htmlFor='lastName' className='register__label'>Last name</label>
              <input type='text' id='lastName' className={this.getFormInputClasses(validation, 'lastName')}
                name='lastName'
                value={lastName}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'lastName')}
            </div>

            <div className='register__form-control'>
              <label htmlFor='password' className='register__label'>Password</label>
              <input type='password' id='password' className={this.getFormInputClasses(validation, 'password')}
                name='password'
                value={password}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'password')}
            </div>

            <div className='register__form-control'>
              <label htmlFor='dateOfBirth' className='register__label'>Date of birth</label>
              <DatePicker
                className={this.getFormInputClasses(validation, 'dateOfBirth')}
                name='dateOfBirth'
                id='dateOfBirth'
                selected={dateOfBirth}
                onChange={this.handleDateChange}
              />
              {this.getValidationError(validation, 'dateOfBirth')}
            </div>

            <div className='register__form-control' role='radiogroup' aria-labelledby='gender'>
              <label id='gender' className='register__label'>Gender</label>
              <div className='register__radio-wrapper'>
                <label className='register__radio-container'>Male
                  <input type='radio' name='gender' value='male' onChange={this.handleGenderChange} checked={gender === 'male'}/>
                  <span className='register__radio-checkmark'></span>
                </label>
                <label className='register__radio-container'>Female
                  <input type='radio' name='gender' value='female' onChange={this.handleGenderChange} checked={gender === 'female'}/>
                  <span className='register__radio-checkmark'></span>
                </label>
                <label className='register__radio-container'>Other
                  <input type='radio' name='gender' value='other' onChange={this.handleGenderChange} checked={gender === 'other'}/>
                  <span className='register__radio-checkmark'></span>
                </label>
              </div>
            </div>

            <button onClick={this.handleFormSubmit} className='register__button'>
          Join The Save Point
            </button>
          </form>
          { displayMessage }
        </div>
      </section>
    );
  }
}

Register.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object
};

export default withRouter(Register);