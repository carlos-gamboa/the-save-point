import React, { Component } from 'react';
import './BillingForm.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormValidator from '../../../helpers/FormValidator';
import BillingHelper from '../../../helpers/BillingHelper';
import Billing from '../../../models/Billing';
import BillingActions from '../../../redux/billing/BillingActions';

class BillingForm extends Component {
  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      { 
        field: 'firstName', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'First Name is required.' 
      },
      { 
        field: 'lastName',
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Last Name is required.'
      },
      { 
        field: 'mobile', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Phone number is required.'
      },
      { 
        field: 'mobile', 
        method: 'isMobilePhone', 
        args: ['en-CA'],
        validWhen: true, 
        message: 'That is not a valid phone number.'
      },
      { 
        field: 'address', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Address is required.'
      },
      { 
        field: 'city', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'City is required.'
      },
      { 
        field: 'county', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'County is required.'
      },
      { 
        field: 'postCode', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Post Code is required.'
      },
      {
        field: 'postCode', 
        method: 'matches',
        args: [/^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/],
        validWhen: true, 
        message: 'That is not a valid postal code.'
      }
    ]);

    this.state = {
      firstName: '',
      lastName: '',
      mobile: '',
      address: '',
      city: '',
      county: '',
      postCode: '',
      validation: this.validator.isValid(),
      message: '',
      showMessage: false,
      messageClass: []
    };

    this.submitted = false;
  }

  componentDidMount() {
    if (this.props.edit) {
      const billingHelper = new BillingHelper();
      billingHelper.getBilling(this.props.match.params.id)
        .then((response) => {
          if (response.status !== 200) {
            throw response;
          }
          return response.json();
        })
        .then((data) => {
          this.setState({
            ...data.data
          });
        })
        .catch((error) => {
          error.json()
            .then((errorResponse) => {
              this.setState({
                message: errorResponse.message,
                showMessage: true,
                messageClass: ['billing-form__message', 'billing-form__message--error']
              });
              this.setMessageTimeout(5000, false);
            });
        });
    }
  }

  handleInputChange = (event) => {
    event.preventDefault();

    this.setState({
      [event.target.name]: event.target.value,
    });
  }
    
  handleFormSubmit = (event) => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      if (this.props.edit) {
        this.updateBilling();
      }
      else {
        this.addBilling();
      }
    }
  }

  addBilling() {
    const billingHelper = new BillingHelper();
    billingHelper.createBilling(new Billing(this.state))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.addBilling(data.data);
        this.setState({
          message: 'The Billing Address has been added.',
          showMessage: true,
          messageClass: ['billing-form__message', 'billing-form__message--success']
        });
        this.setMessageTimeout(3000, true);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['billing-form__message', 'billing-form__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  updateBilling() {
    const billingHelper = new BillingHelper();
    billingHelper.updateBilling(this.props.match.params.id, new Billing(this.state))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.updateBilling(this.props.match.params.id, data.data);
        this.setState({
          message: 'The Billing Address has been updated.',
          showMessage: true,
          messageClass: ['billing-form__message', 'billing-form__message--success']
        });
        this.setMessageTimeout(5000, false);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['billing-form__message', 'billing-form__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  setMessageTimeout = (time, redirect) => {
    setTimeout(() => {
      this.setState({
        message: '',
        showMessage: false,
        messageClass: []
      });
      if (redirect) {
        this.props.history.replace('/account/address');
      }
    }, time);
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['billing-form__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('billing-form__input--invalid');
    }
    return classes.join(' ');
  }

  getValidationError = (validation, fieldToValidate) => {
    const validationError = (validation[fieldToValidate].isInvalid) ?
      <span className='billing-form__validation-error'>{validation[fieldToValidate].message}</span>
      : null;
    
    return validationError;
  }

  render() {
    const { firstName, lastName, mobile, address, city, county, postCode, message, showMessage, messageClass } = this.state;
    const { edit } = this.props;

    const displayMessage = (showMessage) 
      ?
      <div className={messageClass.join(' ')}>
        <p className='billing-form__message-text'>{ message }</p>
      </div>
      : null;

    const title = (edit) ? 'Update Billing Address' : 'New Billing Address';
    const buttonText = (edit) ? 'Update' : 'Add';

    // If the form has been submitted at least once.
    let validation = this.submitted ?                         
      this.validator.validate(this.state) : // Check validity every time we render.
      this.state.validation; // Otherwise just use what's in state

    return (
      <div className='billing-form'>
        <h1 className='billing-form__heading'>{title}</h1>
        <form className='billing-form__form'>
          <div className='billing-form__form-control'>
            <label htmlFor='firstName' className='billing-form__label'>First Name</label>
            <input type='text' className={this.getFormInputClasses(validation, 'firstName')}
              id='firstName'
              name='firstName'
              value={firstName}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'firstName')}
          </div>

          <div className='billing-form__form-control'>
            <label htmlFor='lastName' className='billing-form__label'>Last Name</label>
            <input type='text' className={this.getFormInputClasses(validation, 'lastName')}
              name='lastName'
              id='lastName'
              value={lastName}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'lastName')}
          </div>

          <div className='billing-form__form-control'>
            <label htmlFor='mobile' className='billing-form__label'>Mobile</label>
            <input type='text' className={this.getFormInputClasses(validation, 'mobile')}
              name='mobile'
              id='mobile'
              value={mobile}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'mobile')}
          </div>

          <div className='billing-form__form-control'>
            <label htmlFor='address' className='billing-form__label'>Address</label>
            <input type='text' className={this.getFormInputClasses(validation, 'address')}
              name='address'
              id='address'
              value={address}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'address')}
          </div>

          <div className='billing-form__form-control'>
            <label htmlFor='city' className='billing-form__label'>City</label>
            <input type='text' className={this.getFormInputClasses(validation, 'city')}
              name='city'
              id='city'
              value={city}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'city')}
          </div>

          <div className='billing-form__form-control'>
            <label htmlFor='county' className='billing-form__label'>County</label>
            <input type='text' className={this.getFormInputClasses(validation, 'county')}
              name='county'
              id='county'
              value={county}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'county')}
          </div>

          <div className='billing-form__form-control'>
            <label htmlFor='postCode' className='billing-form__label'>Postal Code</label>
            <input type='text' className={this.getFormInputClasses(validation, 'postCode')}
              name='postCode'
              id='postCode'
              value={postCode}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'postCode')}
          </div>

          <button onClick={this.handleFormSubmit} className='billing-form__button'>{buttonText}</button>
        </form>
        {displayMessage}
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    addBilling: (billing) => {
      dispatch({type: BillingActions.ADD_BILLING, billing});
    },
    updateBilling: (billingId, billing) => {
      dispatch({type: BillingActions.UPDATE_BILLING, payload: {billingId: billingId, billing: billing}});
    }
  };
};

BillingForm.propTypes = {
  edit: PropTypes.bool,
  addBilling: PropTypes.func,
  updateBilling: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object
};

BillingForm.defaultProps = {
  edit: false
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BillingForm));
