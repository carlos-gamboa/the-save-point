import React, { Component } from 'react';
import '../../../../node_modules/card-react/lib/card.css';
import './PaymentForm.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormValidator from '../../../helpers/FormValidator';
import PaymentHelper from '../../../helpers/PaymentHelper';
import Payment from '../../../models/Payment';
import PaymentActions from '../../../redux/payment/PaymentActions';
import CardReactFormContainer from 'card-react';


class PaymentForm extends Component {
  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      { 
        field: 'cardNumber', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Payment Number is required.' 
      },
      { 
        field: 'cardName',
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Payment Name is required.'
      },
      { 
        field: 'expirationDate', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Expiration Date is required.'
      }
    ]);

    this.state = {
      cardNumber: '',
      cardName: '',
      expirationDate: '',
      paymentCVC: '',
      validation: this.validator.isValid(),
      message: '',
      showMessage: false,
      messageClass: []
    };

    this.submitted = false;
  }

  componentDidMount() {
    if (this.props.edit) {
      const paymentHelper = new PaymentHelper();
      paymentHelper.getPayment(this.props.match.params.id)
        .then((response) => {
          if (response.status !== 200) {
            throw response;
          }
          return response.json();
        })
        .then((data) => {
          this.setState({
            ...data.data
          });
        })
        .catch((error) => {
          error.json()
            .then((errorResponse) => {
              this.setState({
                message: errorResponse.message,
                showMessage: true,
                messageClass: ['payment-form__message', 'payment-form__message--error']
              });
              this.setMessageTimeout(5000, false);
            });
        });
    }
  }

  handleInputChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
    
  handleFormSubmit = (event) => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      if (this.props.edit) {
        this.updatePayment();
      }
      else {
        this.addPayment();
      }
    }
  }

  addPayment() {
    const paymentHelper = new PaymentHelper();
    paymentHelper.createPayment(new Payment(this.state))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.addPayment(data.data);
        this.setState({
          message: 'The Payment has been added.',
          showMessage: true,
          messageClass: ['payment-form__message', 'payment-form__message--success']
        });
        this.setMessageTimeout(3000, true);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['payment-form__message', 'payment-form__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  updatePayment() {
    const paymentHelper = new PaymentHelper();
    paymentHelper.updatePayment(this.props.match.params.id, new Payment(this.state))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.updatePayment(this.props.match.params.id, data.data);
        this.setState({
          message: 'The Payment has been updated.',
          showMessage: true,
          messageClass: ['payment-form__message', 'payment-form__message--success']
        });
        this.setMessageTimeout(5000, false);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['payment-form__message', 'payment-form__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  setMessageTimeout = (time, redirect) => {
    setTimeout(() => {
      this.setState({
        message: '',
        showMessage: false,
        messageClass: []
      });
      if (redirect) {
        this.props.history.replace('/account/payment');
      }
    }, time);
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['payment-form__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('payment-form__input--invalid');
    }
    return classes.join(' ');
  }

  getValidationError = (validation, fieldToValidate) => {
    const validationError = (validation[fieldToValidate].isInvalid) ?
      <span className="payment-form__validation-error">{validation[fieldToValidate].message}</span>
      : null;
    
    return validationError;
  }

  render() {
    const { cardNumber, cardName, expirationDate, message, showMessage, messageClass } = this.state;
    const { edit } = this.props;

    const displayMessage = (showMessage) 
      ?
      <div className={messageClass.join(' ')}>
        <p className='payment-form__message-text'>{ message }</p>
      </div>
      : null;

    const title = (edit) ? 'Update Payment Method Info' : 'New Payment Method';
    const buttonText = (edit) ? 'Update' : 'Add';

    // If the form has been submitted at least once.
    let validation = this.submitted ?                         
      this.validator.validate(this.state) : // Check validity every time we render.
      this.state.validation; // Otherwise just use what's in state

    return (
      <div className='payment-form'>
        <h1 className='payment-form__heading'>{title}</h1>
        <div className='payment-form__card-wrapper' id="card-wrapper"></div>
        <CardReactFormContainer
          // The id of the container element where you want to render the card element.
          // the card component can be rendered anywhere (doesn't have to be in ReactCardFormContainer).
          container="card-wrapper"

          // An object contain the form inputs names.
          // every input must have a unique name prop.
          formInputsNames={
            {
              number: 'cardNumber',
              expiry: 'expirationDate',
              cvc: 'paymentCVC',
              name: 'cardName'
            }
          }

          // the class name attribute to add to the input field and the corresponding part of the card element,
          // when the input is valid/invalid.
          classes={
            {
              valid: 'valid-input', // optional — default 'jp-card-valid'
              invalid: 'invalid-input' // optional — default 'jp-card-invalid'
            }
          }

          // specify whether you want to format the form inputs or not
          formatting={true} // optional - default true
        >

          <form className="payment-form__form">
            <div className='payment-form__form-control'>
              <label htmlFor="cardNumber" className='payment-form__label'>Payment Number</label>
              <input type="text" className={this.getFormInputClasses(validation, 'cardNumber')}
                name="cardNumber"
                value={cardNumber}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'cardNumber')}
            </div>

            <div className='payment-form__form-control'>
              <label htmlFor="cardName" className='payment-form__label'>Payment Name</label>
              <input type="text" className={this.getFormInputClasses(validation, 'cardName')}
                name="cardName"
                value={cardName}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'cardName')}
            </div>

            <div className='payment-form__form-control'>
              <label htmlFor="expirationDate" className='payment-form__label'>Expiration Date</label>
              <input type="text" className={this.getFormInputClasses(validation, 'expirationDate')}
                name="expirationDate"
                value={expirationDate}
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'expirationDate')}
            </div>
            <input style={{
              display: 'none'
            }} type="text" 
            name="paymentCVC"
            value={expirationDate}
            onChange={this.handleInputChange}
            />
          </form>

        </CardReactFormContainer>
        <button onClick={this.handleFormSubmit} className="payment-form__button">{buttonText}</button>
        {displayMessage}
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    addPayment: (payment) => {
      dispatch({type: PaymentActions.ADD_CARD, payment});
    },
    updatePayment: (paymentId, payment) => {
      dispatch({type: PaymentActions.UPDATE_CARD, payload: {paymentId: paymentId, payment: payment}});
    }
  };
};

PaymentForm.propTypes = {
  edit: PropTypes.bool,
  addPayment: PropTypes.func,
  updatePayment: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object
};

PaymentForm.defaultProps = {
  edit: false
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PaymentForm));
