import React from 'react';
import './RadioItem.scss';
import PropTypes from 'prop-types';

function RadioItem(props) {

  const handleSelected= () => {
    props.onChangeSelected(props.item.id);
  };

  const spanClassNames = ['radio-item__radio-checkmark'];
  if (props.selected) {
    spanClassNames.push('radio-item__radio-checkmark--active');
  }

  return (
    <div className='radio-item' onClick={(event) => handleSelected(event)}>
      <label className='radio-item__radio-container'>{props.children}
        <input type='radio' name='checkout-step' value={props.item.id} onChange={handleSelected} checked={props.selected}/>
        <span className='radio-item__radio-checkmark'></span>
      </label>
    </div>
  );
}

RadioItem.propTypes = {
  children: PropTypes.object,
  item: PropTypes.object,
  selected: PropTypes.bool,
  onChangeSelected: PropTypes.func
};

export default RadioItem;
