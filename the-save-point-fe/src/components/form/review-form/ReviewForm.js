import React, { Component } from 'react';
import './ReviewForm.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactStars from 'react-stars';
import FormValidator from '../../../helpers/FormValidator';
import ReviewHelper from '../../../helpers/ReviewHelper';
import Review from '../../../models/Review';

class ReviewForm extends Component {
  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      { 
        field: 'title', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Title is required.' 
      },
      { 
        field: 'comment',
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Description is required.'
      }
    ]);

    this.state = {
      title: '',
      comment: '',
      rating: 0,
      validation: this.validator.isValid(),
      message: '',
      showMessage: false,
      messageClass: []
    };

    this.submitted = false;
  }

  handleInputChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
    
  handleFormSubmit = (event) => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      this.addReview();
    }
  }

  handleRatingChanged = (newRating) => {
    this.setState({
      rating: newRating
    });
  }

  addReview() {
    const reviewHelper = new ReviewHelper();
    reviewHelper.addReview(new Review(this.state, this.props.product))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.addReview(data.data);
        this.submitted = false;
        this.setState({
          title: '',
          comment: '',
          rating: 0,
          message: 'Review has been added.',
          showMessage: true,
          messageClass: ['review-form__message', 'review-form__message--success']
        });
        this.setMessageTimeout(3000, true);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['review-form__message', 'review-form__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  setMessageTimeout = (time) => {
    setTimeout(() => {
      this.setState({
        message: '',
        showMessage: false,
        messageClass: []
      });
    }, time);
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['review-form__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('review-form__input--invalid');
    }
    return classes.join(' ');
  }

  getValidationError = (validation, fieldToValidate) => {
    const validationError = (validation[fieldToValidate].isInvalid) ?
      <span className="review-form__validation-error">{validation[fieldToValidate].message}</span>
      : null;
    
    return validationError;
  }

  render() {
    const { title, comment, rating, message, showMessage, messageClass } = this.state;

    const displayMessage = (showMessage) 
      ?
      <div className={messageClass.join(' ')}>
        <p className='review-form__message-text'>{ message }</p>
      </div>
      : null;

    // If the form has been submitted at least once.
    let validation = this.submitted ?                         
      this.validator.validate(this.state) : // Check validity every time we render.
      this.state.validation; // Otherwise just use what's in state

    return (
      <div className='review-form'>
        <h1 className='review-form__heading'>Submit a review</h1>
        <div className='review-form__card-wrapper' id="card-wrapper"></div>
        <form className="review-form__form">
          <div className='review-form__form-control'>
            <label htmlFor="title" className='review-form__label'>Title</label>
            <input type="text" className={this.getFormInputClasses(validation, 'title')}
              id='title'
              name="title"
              value={title}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'title')}
          </div>

          <div className='review-form__form-control'>
            <label htmlFor='comment' className='review-form__label'>Description</label>
            <textarea rows='5' className={this.getFormInputClasses(validation, 'comment')}
              id='comment'
              spellCheck='false'
              name='comment'
              value={comment}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'comment')}
          </div>

          <div className='review-form__form-control'>
            <ReactStars
              className='review-form__stars'
              count={5}
              size={40}
              edit={true}
              value={rating}
              onChange={this.handleRatingChanged}
              color1={'#737373'}
              color2={'#D01345'} />
          </div>

          <button onClick={this.handleFormSubmit} className="review-form__button">Submit</button>
        </form>
        {displayMessage}
      </div>
    );
  }
}

ReviewForm.propTypes = {
  product: PropTypes.object,
  addReview: PropTypes.func
};

export default withRouter(ReviewForm);
