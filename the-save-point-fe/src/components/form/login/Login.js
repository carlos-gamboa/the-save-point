import React, { Component } from 'react';
import './Login.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormValidator from '../../../helpers/FormValidator';
import AuthHelper from '../../../helpers/AuthHelper';
import User from '../../../models/User';
import AuthActions from '../../../redux/auth/AuthActions';
import WishListActions from '../../../redux/wish-list/WishListActions';
import ShoppingCartActions from '../../../redux/shopping-cart/ShoppingCartActions';
const queryString =  require('query-string');

class Login extends Component {
  constructor() {
    super();

    this.validator = new FormValidator([
      { 
        field: 'username', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Email is required.' 
      },
      { 
        field: 'username',
        method: 'isEmail', 
        validWhen: true, 
        message: 'That is not a valid email.'
      },
      { 
        field: 'password', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Password is required.'
      }
    ]);

    this.state = {
      username: '',
      password: '',
      validation: this.validator.isValid(),
      message: '',
      showMessage: false,
      messageClass: []
    };

    this.submitted = false;
  }

  handleInputChange = (event) => {
    event.preventDefault();

    this.setState({
      [event.target.name]: event.target.value,
    });
  }
    
  handleFormSubmit = (event) => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      this.login();
    }
  }

  login() {
    const authHelper = new AuthHelper();
    authHelper.login(new User(this.state))
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        const { firstName, lastName, userId, token, shoppingCart } = data.data;
        
        this.props.setShoppingCart(shoppingCart);
        this.props.getWishList(userId);

        this.props.onLogin(firstName, lastName, userId, token);
        this.setState({
          message: 'You\'ve successfully logged in.',
          showMessage: true,
          messageClass: ['login__message', 'login__message--success']
        });
        this.setMessageTimeout(2000, true);
      })
      .catch((error) => {
        error.json()
          .then((errorResponse) => {
            this.setState({
              message: errorResponse.message,
              showMessage: true,
              messageClass: ['login__message', 'login__message--error']
            });
            this.setMessageTimeout(5000, false);
          });
      });
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['login__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('login__input--invalid');
    }
    return classes.join(' ');
  }

  getValidationError = (validation, fieldToValidate) => {
    const validationError = (validation[fieldToValidate].isInvalid) ?
      <span className="login__validation-error">{validation[fieldToValidate].message}</span>
      : null;
    
    return validationError;
  }

  setMessageTimeout = (time, redirect) => {
    setTimeout(() => {
      this.setState({
        message: '',
        showMessage: false,
        messageClass: []
      });
      if (redirect) {
        const query = queryString.parse(this.props.location.search).redirect;
        if (query) {
          this.props.history.replace('/' + query.split(' ').join('/'));
        }
        else {
          this.props.history.replace('/');
        }
      }
    }, time);
  }

  render() {
    const { username, password, message, showMessage, messageClass } = this.state;
    const displayMessage = (showMessage) 
      ?
      <div className={messageClass.join(' ')}>
        <p className='register__message-text'>{ message }</p>
      </div>
      : null;


    // If the form has been submitted at least once.
    let validation = this.submitted ?                         
      this.validator.validate(this.state) : // Check validity every time we render.
      this.state.validation; // Otherwise just use what's in state

    return (
      <div className='login'>
        <h1 className='login__heading'>Sign in with email</h1>
        <form className="login__form">
          <div className='login__form-control'>
            <label htmlFor="username" className='login__label'>Email address</label>
            <input type="email" id="username" className={this.getFormInputClasses(validation, 'username')}
              name="username"
              value={username}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'username')}
          </div>

          <div className='login__form-control'>
            <label htmlFor="password" className='login__label'>Password</label>
            <input id="password" type="password" className={this.getFormInputClasses(validation, 'password')}
              name="password"
              value={password}
              onChange={this.handleInputChange}
            />
            {this.getValidationError(validation, 'password')}
          </div>

          <button onClick={this.handleFormSubmit} className="login__button">
          Sign in
          </button>
        </form>
        {displayMessage}
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWishList: (userId) => {
      dispatch({type: WishListActions.GET_WISH_LIST, userId});
    },
    setShoppingCart: (items) => {
      dispatch({type: ShoppingCartActions.SET_SHOPPING_CART, items});
    },
    onLogin: (firstName, lastName, userId, token) => {
      dispatch({type: AuthActions.LOGIN, payload: {firstName: firstName, lastName: lastName, userId: userId, token: token}});
    }
  };
};

Login.propTypes = {
  history: PropTypes.object,
  getWishList: PropTypes.func,
  setShoppingCart: PropTypes.func,
  onLogin: PropTypes.func,
  location: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));