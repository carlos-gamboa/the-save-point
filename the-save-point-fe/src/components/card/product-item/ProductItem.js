import React from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';
import ReactStars from 'react-stars';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import WishListActions from '../../../redux/wish-list/WishListActions';

function ProductItem(props) {

  const handleAddShoppingCart = (event) => {
    event.preventDefault();

    if (!props.isLoggedIn) {
      props.history.replace(`/join/login?redirect=product+${product.category}+${product.id}`);
    }
    else {
      if (!props.isOnShoppingCart) {
        props.onAddToShoppingCart(props.product.id);
      }
      else {
        props.history.replace('/my-cart');
      }
    }
  };

  const handleAddWishList = (event) => {
    event.preventDefault();

    props.onAddToWishList(props.product);    
  };

  const { product, isOnShoppingCart, isOnWishList } = props;

  const wishListIconClasses = ['product-item__icon'];
  if (isOnWishList) {
    wishListIconClasses.push('fas');
    wishListIconClasses.push('fa-heart');
  } 
  else {
    wishListIconClasses.push('far');
    wishListIconClasses.push('fa-heart');
  }
  const buttonIcon = (isOnShoppingCart) ? 'check' : 'add_shopping_cart';
   

  return (
    <div className='product-item'>
      <div className='product-item__image-container'>
        <Link to={`/product/${product.category}/${product.id}`} className='product-item__image-wrapper'>
          <img src={product.productPictureLink} alt={product.name} className='product-item__image'/>
        </Link>
        {
          product.leftInStock > 0 ?
            <button aria-label='Add to Shopping Cart' className='product-item__button product-item__button--top' onClick={(event) => handleAddShoppingCart(event)}><i className='material-icons product-item__icon'>{ buttonIcon }</i></button>
            : null
        }
        <button aria-label='Add to Wish List' className='product-item__button' onClick={(event) => handleAddWishList(event)}><i className={wishListIconClasses.join(' ')}></i></button>
      </div>
      <div className='product-item__info'>
        <Link to={`/product/${product.category}/${product.id}`} className='product-item__title'>{product.name}</Link>
        <p className='product-item__price'>${product.price}</p>
        <ReactStars
          className='product-item__stars'
          count={5}
          edit={false}
          value={product.rating}
          color1={'#737373'}
          color2={'#D01345'} />
      </div>      
    </div>
  );
}

const mapStateToProps = (state, props) => {
  return {
    isLoggedIn: state.AuthReducer.isLoggedIn,
    isOnShoppingCart: state.ShoppingCartReducer.shoppingCart.find( item => item.product['id'] === props.product.id ) !== undefined,
    isOnWishList: state.WishListReducer.wishList.find( item => item['id'] === props.product.id ) !== undefined
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToWishList: (item) => {
      dispatch({type: WishListActions.ADD_TO_WISH, item});
    }
  };
};

ProductItem.propTypes = {
  isLoggedIn: PropTypes.bool,
  isOnShoppingCart: PropTypes.bool,
  isOnWishList: PropTypes.bool,
  onAddToWishList: PropTypes.func,
  product: PropTypes.object,
  onAddToShoppingCart: PropTypes.func,
  history: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductItem));
