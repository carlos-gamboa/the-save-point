import React from 'react';
import './SimpleCard.scss';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function SimpleCard(props) {

  const handleDelete= (event) => {
    event.preventDefault();
    props.onDelete(props.itemId);
  };

  const { itemId, match, children } = props;
  return (
    <div className='simple-card'>
      <div className='simple-card__info'>
        { children }
      </div>
      <div className='simple-card__actions'>
        <Link to={match.url + '/' + itemId + '/edit'} className='simple-card__button' aria-label='Edit'><i className='material-icons simple-card__icon simple-card__icon--m0' aria-hidden='true'>edit</i></Link>
        <button className='simple-card__button' aria-label='Delete'><i className='material-icons simple-card__icon simple-card__icon--m0' onClick={(event) => handleDelete(event)} aria-hidden='true'>delete</i></button>
      </div>
    </div>
  );
}

SimpleCard.propTypes = {
  children: PropTypes.object,
  itemId: PropTypes.number,
  onDelete: PropTypes.func,
  match: PropTypes.object
};

export default SimpleCard;
