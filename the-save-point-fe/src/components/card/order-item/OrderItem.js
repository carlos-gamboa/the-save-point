import React from 'react';
import './OrderItem.scss';
import PropTypes from 'prop-types';
import DateHelper from '../../../helpers/DateHelper';

function OrderItem(props) {

  const { order } = props;

  const getOrderTitle = () => {
    const dateHelper = new DateHelper();
    const date = dateHelper.getDateFromSQLDate(order.arrivingDate);
    const today = new Date();
    let orderTitle;

    if (dateHelper.areSameDate(date, today)) {
      orderTitle = 'It\'ll arrive anytime!';
    }
    else if (date > today) {
      orderTitle = 'We\'ve sent it!';
    }
    else {
      orderTitle = 'It\'s been delivered!';
    }

    return orderTitle;
  };

  const getOrderDate = () => {
    const dateHelper = new DateHelper();
    const date = dateHelper.getDateFromSQLDate(order.arrivingDate);
    const today = new Date();
    let arrivingTitle;

    if (dateHelper.areSameDate(date, today)) {
      arrivingTitle = 'Estimated delivery: Today';
    }
    else if (date > today) {
      arrivingTitle = `Estimated delivery: ${dateHelper.getDayName(date.getDay())}, ${date.getDate()} ${dateHelper.getMonthName(date.getMonth())}`;
    }
    else {
      arrivingTitle = `Delivered on: ${dateHelper.getDayName(date.getDay())}, ${date.getDate()} ${dateHelper.getMonthName(date.getMonth())}`;
    }

    return arrivingTitle;
  };

  const getOrderItemClasses = () => {
    const dateHelper = new DateHelper();
    const date = dateHelper.getDateFromSQLDate(order.arrivingDate);
    const today = new Date();
    let orderItemClasses = ['order-item'];

    if (dateHelper.areSameDate(date, today)) {
      orderItemClasses.push('order-item--today');
    }
    else if (date < today) {
      orderItemClasses.push('order-item--delivered');
    }

    return orderItemClasses.join(' ');
  };

  return (
    <div className={getOrderItemClasses()}>
      <div className='order-item__top'>
        <p className='order-item__title'>{getOrderTitle()}</p>
        <p className='order-item__text'>{getOrderDate()}</p>
        <p className='order-item__text'>To {order.billing.address}</p>
      </div>
      <span className='order-item__bar'></span>
      <div className='order-item__bottom'>
        {
          order.shoppingCart.map(item => {
            return (
              <p key={item.id} className='order-item__product-info'>{item.quantity} &mdash; {item.product.name}</p>
            );
          })
        }
      </div>
    </div>
  );
}

OrderItem.propTypes = {
  order: PropTypes.object
};

export default OrderItem;
