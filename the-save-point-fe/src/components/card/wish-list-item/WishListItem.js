import React, { Component } from 'react';
import './WishListItem.scss';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';

class WishListItem extends Component  {

  handleRemoveFromWishList = (event) => {
    event.preventDefault();
    this.props.onRemoveFromWishList(this.props.item.id);
  };

  handleAddToShoppingCart = (event) => {
    event.preventDefault();
    this.props.onAddToShoppingCart(this.props.item.id);
  };

  getStockText = (leftInStock) => {
    let stock;
    if (leftInStock > 1) {
      stock = <React.Fragment>Only <span className='product__left-stock--bold'>{leftInStock}</span> left in stock</React.Fragment>;
    }
    else {
      stock = <React.Fragment>Out of stock</React.Fragment>;
    }
    return stock;
  }

  render() {
    const { item } = this.props;
    return (
      <div className='wish-list-item'>
        <div className='wish-list-item__image-container'>
          <Link to={`/product/${item.category}/${item.id}`} className='wish-list-item__image-wrapper'>
            <img src={item.productPictureLink} alt={item.name} className='wish-list-item__image'/>
          </Link>
        </div>
        <div className='wish-list-item__info'>
          <Link to={`/product/${item.category}/${item.id}`} className='wish-list-item__title'>{item.name}</Link>
          <button className='wish-list-item__button wish-list-item__button--remove' onClick={(event) => this.handleRemoveFromWishList(event)}><i className='material-icons wish-list-item__icon wish-list-item__icon--small'>close</i></button>
          <p className='product__left-stock'>{this.getStockText(item.leftInStock)}</p>
          <p className='wish-list-item__price'>${item.price}</p>
          {
            item.leftInStock > 0 ? 
              <button className='wish-list-item__button' onClick={(event) => this.handleAddToShoppingCart(event)}>Add to shopping cart</button>
              : null
          }
          
        </div>
      </div>
    );
  }
}

WishListItem.propTypes = {
  item: PropTypes.object,
  onRemoveFromWishList: PropTypes.func,
  onAddToShoppingCart: PropTypes.func
};

export default withRouter(WishListItem);
