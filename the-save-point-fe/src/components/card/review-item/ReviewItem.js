import React from 'react';
import './ReviewItem.scss';
import PropTypes from 'prop-types';
import ReactStars from 'react-stars';

function ReviewItem(props) {
  const { review } = props;
  return (
    <div className='review-item'>
      <p className='review-item__title'>{review.title}</p>
      <p className='review-item__user'>By {review.user.firstName} {review.user.lastName}</p>
      <p className='review-item__description'>{review.comment}</p>
      <ReactStars
        className='review-item__stars'
        count={5}
        edit={false}
        size={35}
        value={review.rating}
        color1={'#737373'}
        color2={'#D01345'} />
    </div>
  );
}

ReviewItem.propTypes = {
  review: PropTypes.object
};

export default ReviewItem;
