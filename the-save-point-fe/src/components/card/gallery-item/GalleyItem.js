import React from 'react';
import './GalleryItem.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function GalleyItem(props) {

  const { product } = props;
  return (
    <Link to={`/product/${product.category}/${product.id}`} className='gallery-item'>
      <div className='gallery-item__image-wrapper'>
        <img src={product.productPictureLink} alt={'Cover of ' + product.name} className='gallery-item__image'/>
      </div>
      <p className='gallery-item__title'>{product.name}</p>
    </Link>
  );
}

GalleyItem.propTypes = {
  product: PropTypes.object
};

export default withRouter(GalleyItem);
