import React, { Component } from 'react';
import './ShoppingCartItem.scss';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';

class ShoppingCartItem extends Component  {

  handleRemoveFromShoppingCart = (event) => {
    event.preventDefault();
    this.props.onRemoveFromShoppingCart(this.props.item.id);
  };

  handleIncreaseProductQuantity = (event) => {
    event.preventDefault();
    this.props.onUpdateProductQuantity(this.props.item.id, this.props.item.quantity + 1);
  };

  handleDecreaseProductQuantity = (event) => {
    event.preventDefault();
    if (this.props.item.quantity > 1) {
      this.props.onUpdateProductQuantity(this.props.item.id, this.props.item.quantity - 1);
    }
  };

  render() {
    const { item } = this.props;
    return (
      <div className='shopping-cart-item'>
        <div className='shopping-cart-item__image-container'>
          <Link to={`/product/${item.product.category}/${item.product.id}`} className='shopping-cart-item__image-wrapper'>
            <img src={item.product.productPictureLink} alt={item.product.name} className='shopping-cart-item__image'/>
          </Link>
        </div>
        <div className='shopping-cart-item__info'>
          <Link to={`/product/${item.product.category}/${item.product.id}`} className='shopping-cart-item__title'>{item.product.name}</Link>
          <button className='shopping-cart-item__button shopping-cart-item__button--remove' onClick={(event) => this.handleRemoveFromShoppingCart(event)}><i className='material-icons shopping-cart-item__icon shopping-cart-item__icon--small'>close</i></button>
          <p className='shopping-cart-item__price'>${(item.product.price * item.quantity).toFixed(2)}{ item.quantity > 1 ?<span className='shopping-cart-item__price--single'>(${item.product.price} each)</span> : null}</p>
          <div className='shopping-cart-item__actions'>
            <div className='shopping-cart-item__quantity-wrapper'>
              <button className='shopping-cart-item__button shopping-cart-item__button--quantity' onClick={(event) => this.handleDecreaseProductQuantity(event)}><i className='material-icons shopping-cart-item__icon shopping-cart-item__icon--small'>remove</i></button>
              <p className='shopping-cart-item__quantity'>{item.quantity}</p>
              <button className='shopping-cart-item__button shopping-cart-item__button--quantity' onClick={(event) => this.handleIncreaseProductQuantity(event)}><i className='material-icons shopping-cart-item__icon shopping-cart-item__icon--small'>add</i></button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ShoppingCartItem.propTypes = {
  item: PropTypes.object,
  onRemoveFromShoppingCart: PropTypes.func,
  onUpdateProductQuantity: PropTypes.func
};

export default withRouter(ShoppingCartItem);
