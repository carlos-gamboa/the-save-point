import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import scriptLoader from 'react-async-script-loader';
import ProductHelper from '../../../helpers/ProductHelper';

class PaypalButton extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      showButton: false,
    };

    window.React = React;
    window.ReactDOM = ReactDOM;
  }

  componentDidMount = () => {
    const {
      isScriptLoaded,
      isScriptLoadSucceed
    } = this.props;

    // If the Paypal script was already loaded, show the button
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ showButton: true });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const {
      isScriptLoaded,
      isScriptLoadSucceed,
    } = nextProps;

    const isLoadedButWasntLoadedBefore =
      !this.state.showButton &&
      !this.props.isScriptLoaded &&
      isScriptLoaded;

    // If the Paypal script was loading just now, show the button.
    if (isLoadedButWasntLoadedBefore) {
      if (isScriptLoadSucceed) {
        this.setState({ showButton: true });
      }
    }
  }
  
  render() {
    const {
      total,
      currency,
      env,
      commit,
      client,
      onSuccess,
      onError,
      onOrderExpired,
      orderId
    } = this.props;

    const {
      showButton,
    } = this.state;

    const paypal = window.PAYPAL;

    const style = {
      layout: 'horizontal', // horizontal | vertical
      size: 'responsive', // medium | large | responsive
      shape: 'rect', // pill | rect
      color: 'white' // gold | blue | silver | white | black
    };

    let ReactButton = (showButton) ? paypal.Button.driver('react', {
      React: window.React,
      ReactDOM: window.ReactDOM
    }) : null ;

    const payment = () =>
      paypal.rest.payment.create(env, client, {
        transactions: [
          {
            amount: {
              total,
              currency,
            }
          },
        ],
      });

    const onAuthorize = (data, actions) => {
      const productHelper = new ProductHelper();
      productHelper.getOrder(orderId)
        .then((response) => {
          if (response.status !== 200) {
            throw response;
          }
          return response.json();
        })
        .then((data) => {
          if (data.data.status === 'pending') {
            actions.payment.execute()
              .then(() => {
                const payment = {
                  paid: true,
                  cancelled: false,
                  payerID: data.payerID,
                  paymentID: data.paymentID,
                  paymentToken: data.paymentToken,
                  returnUrl: data.returnUrl,
                };
    
                onSuccess(payment);
              });
          } else {
            onOrderExpired('Looks like your order expired. Please try again!');
          }
        })
        .catch(() => {
          onOrderExpired('Looks like your order expired. Please try again!');
        });
    };
      

    return (
      <div>
        {showButton && <ReactButton
          style={style}
          env={env}
          client={client}
          commit={commit}
          payment={payment}
          onAuthorize={onAuthorize}
          onError={onError}
        />}
      </div>
    );
  }
}

PaypalButton.propTypes = {
  isScriptLoaded: PropTypes.bool,
  isScriptLoadSucceed: PropTypes.bool,
  total: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  env: PropTypes.string.isRequired,
  commit: PropTypes.bool.isRequired,
  client: PropTypes.object.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onOrderExpired: PropTypes.func.isRequired,
  orderId: PropTypes.number.isRequired
};

export default scriptLoader('https://www.paypalobjects.com/api/checkout.js')(PaypalButton);