export default class User {
  id;
  username;
  password;
  firstName;
  lastName;
  dateOfBirth;
  gender;
  profilePictureLink;

  constructor(userInfo) {
    this.id = userInfo.id;
    this.username = userInfo.username;
    this.password = userInfo.password;
    this.firstName = userInfo.firstName;
    this.lastName = userInfo.lastName;
    this.dateOfBirth = new Date(userInfo.dateOfBirth);
    this.gender = userInfo.gender;
    this.profilePictureLink = userInfo.profilePictureLink;
  }
}