export default class Payment {
  id;
  cardNumber;
  expirationDate;
  cardName;

  constructor(paymentInfo) {
    this.id = paymentInfo.id;
    this.cardNumber = paymentInfo.cardNumber;
    this.expirationDate = paymentInfo.expirationDate;
    this.cardName = paymentInfo.cardName;
  }
}