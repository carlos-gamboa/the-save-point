export default class Review {
  id;
  title;
  comment;
  rating;
  product;

  constructor(revieInfo, product) {
    this.id = revieInfo.id;
    this.title = revieInfo.title;
    this.comment = revieInfo.comment;
    this.rating = revieInfo.rating;
    this.product = product;
  }
}