export default class Billing {
  id;
  firstName;
  lastName;
  mobile;
  address;
  city;
  county;
  postCode;

  constructor(billingInfo) {
    this.id = billingInfo.id;
    this.firstName = billingInfo.firstName;
    this.lastName = billingInfo.lastName;
    this.mobile = billingInfo.mobile;
    this.address = billingInfo.address;
    this.city = billingInfo.city;
    this.county = billingInfo.county;
    this.postCode = billingInfo.postCode;
  }
}