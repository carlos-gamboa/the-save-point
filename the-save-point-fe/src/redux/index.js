import { combineReducers } from 'redux';
import AuthReducer from './auth/AuthReducer';
import BillingReducer from './billing/BillingReducer';
import PaymentReducer from './payment/PaymentReducer';
import ShoppingCartReducer from './shopping-cart/ShoppingCartReducer';
import CheckoutReducer from './checkout/CheckoutReducer';
import WishListReducer from './wish-list/WishListReducer';

export default combineReducers({
  AuthReducer,
  BillingReducer,
  PaymentReducer,
  ShoppingCartReducer,
  CheckoutReducer,
  WishListReducer
});