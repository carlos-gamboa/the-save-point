import BillingActions from './BillingActions';

const INITIAL_STATE = {
  billings: []
};

export default function BillingReducer(state = INITIAL_STATE, action) {
  let newState, billingIndex, newBillings;
  switch (action.type) {
  case BillingActions.ADD_BILLING:
    newBillings = [...state.billings];
    newBillings.push(action.billing);
    newState = {...state};
    newState.billings = newBillings;
    break;

  case BillingActions.UPDATE_BILLING:
    billingIndex = state.billings.findIndex(billing => billing.id === action.payload.billingId);
    newBillings = [...state.billings];
    newBillings[billingIndex] = action.payload.billing;
    newState = {...state};
    newState.billings = newBillings;
    break;

  case BillingActions.DELETE_BILLING:
    billingIndex = state.billings.findIndex(billing => billing.id === action.billingId);
    newBillings = [...state.billings];
    newBillings.splice(billingIndex, 1);
    newState = {...state};
    newState.billings = newBillings;
    break;

  case BillingActions.SET_BILLINGS:
    newState = {...state};
    newState.billings = [...action.billings];
    break;

  default:
    newState = {...state};
    break;
  }

  return Object.assign(
    {},
    state,
    {
      ...newState
    }
  );
}