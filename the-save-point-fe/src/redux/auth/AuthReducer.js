import AuthActions from './AuthActions';

const INITIAL_STATE = {
  firstName: '',
  lastName: '',
  userId: null,
  token: null,
  isLoggedIn: false
};

export default function AuthReducer(state = INITIAL_STATE, action) {
  let newState, token;
  switch (action.type) {

  case AuthActions.LOGIN:
    token = action.payload.token;
    localStorage.setItem('token', token);
    newState = state;
    newState.token = token;
    newState.firstName = action.payload.firstName;
    newState.lastName = action.payload.lastName;
    newState.userId = action.payload.userId;
    newState.isLoggedIn = true;
    break;

  case AuthActions.LOGOUT:
    localStorage.removeItem('token');
    newState = state;
    newState.isLoggedIn = false;
    newState.token = '';
    newState.firstName = '';
    newState.lastName = '';
    newState.userId = null;
    break;

  default:
    token = localStorage.getItem('token');   
    if (token) {
      newState = state;
      newState.token = token;
      newState.isLoggedIn = true;
    } 
    break;
  }

  return Object.assign(
    {},
    state,
    {
      ...newState
    }
  );
}