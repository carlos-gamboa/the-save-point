const PaymentActions = {
  ADD_CARD: 'ADD_CARD',
  UPDATE_CARD: 'UPDATE_CARD',
  DELETE_CARD: 'DELETE_CARD',
  SET_CARDS: 'SET_CARDS'
};

export default PaymentActions;