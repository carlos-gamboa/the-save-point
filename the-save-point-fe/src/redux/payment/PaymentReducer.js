import PaymentActions from './PaymentActions';

const INITIAL_STATE = {
  payments: []
};

export default function PaymentReducer(state = INITIAL_STATE, action) {
  let newState, paymentIndex, newPayments;
  switch (action.type) {
  case PaymentActions.ADD_CARD:
    newPayments = [...state.payments];
    newPayments.push(action.payment);
    newState = {...state};
    newState.payments = newPayments;
    break;

  case PaymentActions.UPDATE_CARD:
    paymentIndex = state.payments.findIndex(payment => payment.id === action.payload.paymentId);
    newPayments = [...state.payments];
    newPayments[paymentIndex] = action.payload.payment;
    newState = {...state};
    newState.payments = newPayments;
    break;

  case PaymentActions.DELETE_CARD:
    paymentIndex = state.payments.findIndex(payment => payment.id === action.paymentId);
    newPayments = [...state.payments];
    newPayments.splice(paymentIndex, 1);
    newState = {...state};
    newState.payments = newPayments;
    break;

  case PaymentActions.SET_CARDS:
    newState = {...state};
    newState.payments = [...action.payments];
    break;

  default:
    newState = {...state};
    break;
  }

  return Object.assign(
    {},
    state,
    {
      ...newState
    }
  );
}