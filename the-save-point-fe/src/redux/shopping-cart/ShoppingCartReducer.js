import ShoppingCartActions from './ShoppingCartActions';

const INITIAL_STATE = {
  shoppingCart: []
};

export default function ShoppingCartReducer(state = INITIAL_STATE, action) {
  let newState, itemIndex, newShoppingCart;
  switch (action.type) {
  case ShoppingCartActions.SET_SHOPPING_CART:
    newShoppingCart = [...action.items];
    newState = {...state};
    newState.shoppingCart = newShoppingCart;
    break;

  case ShoppingCartActions.ADD_TO_CART:
    newShoppingCart = [...state.shoppingCart];
    itemIndex = state.shoppingCart.findIndex(item => item.product.id === action.item.product.id);
    if (itemIndex === -1) {
      newShoppingCart.push(action.item);
    } 
    else {
      newShoppingCart[itemIndex] = action.item;
    }
    newState = {...state};
    newState.shoppingCart = newShoppingCart;
    break;

  case ShoppingCartActions.DELETE_FROM_CART:
    itemIndex = state.shoppingCart.findIndex(item => item.id === action.itemId);
    newShoppingCart = [...state.shoppingCart];
    newShoppingCart.splice(itemIndex, 1);
    newState = {...state};
    newState.shoppingCart = newShoppingCart;
    break;

  case ShoppingCartActions.UPDATE_ITEM_QUANTITY:
    itemIndex = state.shoppingCart.findIndex(item => item.id === action.payload.itemId);
    newShoppingCart = [...state.shoppingCart];
    newShoppingCart[itemIndex].quantity = action.payload.itemQuantity;
    newState = {...state};
    newState.shoppingCart = newShoppingCart;
    break;

  case ShoppingCartActions.CHECKOUT:
    newShoppingCart = [];
    newState = {...state};
    newState.shoppingCart = newShoppingCart;
    break;

  default:
    newState = {...state};
    break;
  }

  return Object.assign(
    {},
    state,
    {
      ...newState
    }
  );
}