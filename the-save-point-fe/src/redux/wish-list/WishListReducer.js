import WishListActions from './WishListActions';

const INITIAL_STATE = {
  wishList: [],
  userKey: 'wl-anon'
};

const mergeWishLists = (anonWishList, userWishList) => {
  for (let anonItem of anonWishList) {
    if (userWishList.find( userItem => userItem.id === anonItem.id ) === undefined) {
      userWishList.push(anonItem);
    }
  }
  return userWishList;
};

export default function WishListReducer(state = INITIAL_STATE, action) {
  let newState, itemIndex, newWishList, anonWishList;
  switch (action.type) {
    
  case WishListActions.SET_ANON:
    newWishList = JSON.parse(localStorage.getItem('wl-anon'));
    if (newWishList === null) {
      localStorage.setItem('wl-anon', JSON.stringify([]));
      newWishList = [];
    }
    newState = {...state};
    newState.wishList = newWishList;
    newState.userKey = 'wl-anon';
    break;
    
  case WishListActions.RESET_ANON:
    newWishList = [];
    newState = {...state};
    newState.wishList = newWishList;
    newState.userKey = 'wl-anon';
    localStorage.setItem(newState.userKey, JSON.stringify(newWishList));
    break;

  case WishListActions.GET_WISH_LIST:
    anonWishList = JSON.parse(localStorage.getItem('wl-anon'));
    newWishList = JSON.parse(localStorage.getItem(`wl-${action.userId}`));
    if (newWishList === null) {
      newWishList = [];
    }
    if (anonWishList !== null) {
      newWishList = mergeWishLists(anonWishList, newWishList);
    }
    newState = {...state};
    newState.wishList = newWishList;
    newState.userKey = `wl-${action.userId}`;
    localStorage.setItem(newState.userKey, JSON.stringify(newWishList));
    localStorage.setItem('wl-anon', JSON.stringify([]));
    break;

  case WishListActions.ADD_TO_WISH:
    newWishList = [...state.wishList];
    newWishList.push(action.item);
    newState = {...state};
    newState.wishList = newWishList;
    localStorage.setItem(state.userKey, JSON.stringify(newWishList));
    break;

  case WishListActions.DELETE_FROM_WISH:
    itemIndex = state.wishList.findIndex(item => item.id === action.itemId);
    newWishList = [...state.wishList];
    newWishList.splice(itemIndex, 1);
    newState = {...state};
    newState.wishList = newWishList;
    localStorage.setItem(state.userKey, JSON.stringify(newWishList));
    break;

  case WishListActions.REMOVE_ALL:
    newWishList = [];
    newState = {...state};
    newState.wishList = newWishList;
    localStorage.setItem(state.userKey, JSON.stringify(newWishList));
    break;

  default:
    newState = {...state};
    break;
  }

  return Object.assign(
    {},
    state,
    {
      ...newState
    }
  );
}