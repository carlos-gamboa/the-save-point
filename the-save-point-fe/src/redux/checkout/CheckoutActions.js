const CheckoutActions = {
  SET_BILLING: 'SET_BILLING',
  SET_PAYMENT: 'SET_PAYMENT',
  SET_PRODUCTS: 'SET_PRODUCTS',
  SET_STEP: 'SET_STEP',
  SET_ORDER: 'SET_ORDER'
};

export default CheckoutActions;