import CheckoutActions from './CheckoutActions';

const INITIAL_STATE = {
  billingId: null,
  paymentId: 0,
  step: 1,
  products: [],
  order: null
};

export default function CheckoutReducer(state = INITIAL_STATE, action) {
  let newState;
  switch (action.type) {
  case CheckoutActions.SET_BILLING:
    newState = {...state};
    newState.billingId = action.billingId;
    break;

  case CheckoutActions.SET_PAYMENT:
    newState = {...state};
    newState.paymentId = action.paymentId;
    break;

  case CheckoutActions.SET_PRODUCTS:
    newState = {...state};
    newState.products = action.products;
    break;

  case CheckoutActions.SET_STEP:
    newState = {...state};
    newState.step = action.step;
    break;

  case CheckoutActions.SET_ORDER:
    newState = {...state};
    newState.order = action.order;
    break;

  default:
    newState = {...state};
    break;
  }

  return Object.assign(
    {},
    state,
    {
      ...newState
    }
  );
}