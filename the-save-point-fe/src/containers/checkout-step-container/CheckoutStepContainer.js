import React, { Component } from 'react';
import './CheckoutStepContainer.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BillingHelper from '../../helpers/BillingHelper';
import CheckoutActions from '../../redux/checkout/CheckoutActions';
import RadioItem from '../../components/form/radio-item/RadioItem';
import PaymentHelper from '../../helpers/PaymentHelper';
import Loading from '../../components/loading/Loading';

class CheckoutStepContainer extends Component {

  constructor() {
    super();

    this.state = {
      items: [],
      loading: true
    };
  }

  componentDidMount() {
    if (this.props.type === 'billing') {
      this.getAllBillings();
    } else {
      this.getAllPayments();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.type !== this.props.type) {
      this.setState({
        items: [],
        loading: true
      });
      if (this.props.type === 'billing') {
        this.getAllBillings();
      } else {
        this.getAllPayments();
      }
    }
  }

  getAllBillings = () => {
    const billingHelper = new BillingHelper();
    billingHelper.getAllBillings()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          items: data.data,
          loading: false
        });
      })
      .catch(() => {
        this.setState({
          items: [],
          loading: false
        });
      });
  }

  getAllPayments = () => {
    const paymentHelper = new PaymentHelper();
    paymentHelper.getAllPayments()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          items: data.data,
          loading: false
        });
      })
      .catch(() => {
        this.setState({
          items: [],
          loading: false
        });
      });
  }

  formatCardNumber = (cardNumber) => {
    if (!cardNumber) {
      return null;
    }
    return `(${cardNumber.substring(cardNumber.length - 4, cardNumber.length)})`;
  };

  onChangeSelected = (id) => {
    if (this.props.type === 'billing') {
      this.props.setBilling(id);
    }
    else {
      this.props.setPayment(id);
    }
  }

  handleContinueClick = (event) => {
    event.preventDefault();
    this.props.onContinueClicked();
  }

  handleBackClick = (event) => {
    event.preventDefault();
    this.props.onBackClicked();
  }

  render() {
    const { items, loading } = this.state;
    const { step, selectedBilling, selectedPayment, type } = this.props;

    const redirectMessage = (type === 'billing') ? 'billing address' : 'payment method';
    const redirectLocation = (type === 'billing') ? '/account/address/new' : '/account/payment/new';
    const redirectButtonText = (type === 'billing') ? 'Add new billing address' : 'Add new payment method';

    const optionPaypal = 
    <RadioItem
      key={0}
      item={{id: 0}} 
      selected={0 === selectedPayment}
      onChangeSelected={this.onChangeSelected.bind(this)}
    >
      <div className='radio-item__text-wrapper'>
        <i className='fab fa-paypal radio-item__icon'></i>
        <p className='radio-item__text'>Paypal</p>
      </div>
    </RadioItem>;
    

    const itemContent = (items.length === 0) ? 
      <div className='checkout-step-container__error-wrapper'>
        <p className='checkout-step-container__error'>Looks like you don&apos;t have any {redirectMessage}, add one before doing the checkout</p>
        <Link to={redirectLocation} className='checkout-step-container__button checkout-step-container__button--big'>{redirectButtonText}</Link>
      </div>
      : 
      <React.Fragment>
        <div className='checkout-step-container__wrapper'>
          { type === 'payment' ? optionPaypal : null}
          { items.map((item) => {
            return (
              <RadioItem
                key={item.id}
                item={item} 
                selected={(type === 'billing') ? (item.id === selectedBilling) : (item.id === selectedPayment)}
                onChangeSelected={this.onChangeSelected.bind(this)}
              >
                {(type === 'billing') ?
                  <React.Fragment>
                    <div className='radio-item__text-wrapper'>
                      <i className='material-icons radio-item__icon' aria-hidden='true'>person</i>
                      <p className='radio-item__text'><span className='radio-item__label'>Full Name:</span>{item.firstName} {item.lastName}</p>
                    </div>
                    <div className='radio-item__text-wrapper'>
                      <i className='material-icons radio-item__icon' aria-hidden='true'>local_shipping</i>
                      <p className='radio-item__text'><span className='radio-item__label'>Address:</span>{item.address}</p>
                    </div>
                    <div className='radio-item__text-wrapper'>
                      <i className='material-icons radio-item__icon' aria-hidden='true'>location_city</i>
                      <p className='radio-item__text'><span className='radio-item__label'>Location:</span>{item.city}, {item.county}</p>
                    </div>
                    <div className='radio-item__text-wrapper'>
                      <i className='material-icons radio-item__icon' aria-hidden='true'>markunread_mailbox</i>
                      <p className='radio-item__text'><span className='radio-item__label'>Postal Code:</span>{item.postCode}</p>
                    </div>
                    <div className='radio-item__text-wrapper'>
                      <i className='material-icons radio-item__icon' aria-hidden='true'>phone_iphone</i>
                      <p className='radio-item__text'><span className='radio-item__label'>Mobile:</span>{item.mobile}</p>
                    </div>
                  </React.Fragment> 
                  :
                  <React.Fragment>
                    <div className='radio-item__text-wrapper'>
                      <i className='material-icons radio-item__icon' aria-hidden='true'>person</i>
                      <p className='radio-item__text'>{item.cardName}</p>
                    </div>
                    <div className='radio-item__text-wrapper' >
                      <i className='material-icons radio-item__icon' aria-hidden='true'>credit_card</i>
                      <p className='radio-item__text'>{this.formatCardNumber(item.cardNumber)}</p>
                    </div>
                    <div className='radio-item__text-wrapper'>
                      <p className='radio-item__text'>Expiration Date: {item.expirationDate}</p>
                    </div>
                  </React.Fragment>
                }
              </RadioItem>
            );
          })}
        </div>
        <div className='checkout-step-container__actions'>
          { step !== 1 ? <button className='checkout-step-container__button' onClick={(event) => this.handleBackClick(event)}>Back</button> : null}
          <button className='checkout-step-container__button' disabled={(step === 1) ? (selectedBilling === null) : (selectedPayment === null)} onClick={(event) => this.handleContinueClick(event)}>Continue</button>
        </div>
      </React.Fragment>;
    ;

    const content = (loading && items.length === 0) ?
      <div className='checkout-step-container__loading'><Loading></Loading></div>
      :
      <React.Fragment>
        { itemContent }
      </React.Fragment>;


    return (
      <div className='checkout-step-container'>
        <h2 className='checkout-step-container__heading'>Choose a {type === 'billing' ? 'Billing Address' : 'Payment Method'}</h2>
        { content }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedBilling: state.CheckoutReducer.billingId,
    selectedPayment: state.CheckoutReducer.paymentId
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setBilling: (billingId) => {
      dispatch({type: CheckoutActions.SET_BILLING, billingId});
    },
    setPayment: (paymentId) => {
      dispatch({type: CheckoutActions.SET_PAYMENT, paymentId});
    }
  };
};

CheckoutStepContainer.propTypes = {
  type: PropTypes.string,
  selectedBilling: PropTypes.number,
  selectedPayment: PropTypes.number,
  setBilling: PropTypes.func,
  setPayment: PropTypes.func,
  step: PropTypes.number,
  onContinueClicked: PropTypes.func,
  onBackClicked: PropTypes.func
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckoutStepContainer));
