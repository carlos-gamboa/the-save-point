import React, { Component } from 'react';
import './BillingContainer.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SimpleCard from '../../components/card/simple-card/SimpleCard';
import BillingHelper from '../../helpers/BillingHelper';
import BillingActions from '../../redux/billing/BillingActions';
import Loading from '../../components/loading/Loading';

class BillingContainer extends Component {

  constructor() {
    super();

    this.state = {
      loading: true
    };
  }

  componentDidMount = () => {
    const billingHelper = new BillingHelper();
    billingHelper.getAllBillings()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setBillings(data.data);
        this.setState({
          loading: false
        });
      })
      .catch(() => {
        this.setState({
          loading: false
        });
      });
  }

  onDelete = (id) => {
    const billingHelper = new BillingHelper();
    billingHelper.deleteBilling(id)
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        this.props.deleteBilling(id);
      });
  }

  render = () => {
    const { loading } = this.state;
    const { billings } = this.props;

    const content = (loading) ?
      <div className='order-container__loading-wrapper'>
        <Loading></Loading> 
      </div>
      :
      <div className='billing-container__wrapper'>
        { billings.map((billing) => {
          return (
            <SimpleCard 
              key={billing.id}
              itemId={billing.id} 
              onDelete={this.onDelete.bind(this)} 
              match={this.props.match}
            >
              <React.Fragment>
                <div className='simple-card__text-wrapper'>
                  <i className='material-icons simple-card__icon' aria-hidden='true'>person</i>
                  <p className='simple-card__text'><span className='simple-card__label'>Full Name:</span>{billing.firstName} {billing.lastName}</p>
                </div>
                <div className='simple-card__text-wrapper'>
                  <i className='material-icons simple-card__icon' aria-hidden='true'>local_shipping</i>
                  <p className='simple-card__text'><span className='simple-card__label'>Address:</span>{billing.address}</p>
                </div>
                <div className='simple-card__text-wrapper'>
                  <i className='material-icons simple-card__icon' aria-hidden='true'>location_city</i>
                  <p className='simple-card__text'><span className='simple-card__label'>Location:</span>{billing.city}, {billing.county}</p>
                </div>
                <div className='simple-card__text-wrapper'>
                  <i className='material-icons simple-card__icon' aria-hidden='true'>markunread_mailbox</i>
                  <p className='simple-card__text'><span className='simple-card__label'>Postal Code:</span>{billing.postCode}</p>
                </div>
                <div className='simple-card__text-wrapper'>
                  <i className='material-icons simple-card__icon' aria-hidden='true'>phone_iphone</i>
                  <p className='simple-card__text'><span className='simple-card__label'>Mobile:</span>{billing.mobile}</p>
                </div>
              </React.Fragment>
              
            </SimpleCard>
          );
        })}
      </div>;

    return (
      <div className='billing-container'>
        <div className='billing-container__top'>
          <h2 className='billing-container__heading'>Address Book</h2>
          <Link to={this.props.match.url + '/new'} className='billing-container__button'>Add new Address</Link>
        </div>
        { content }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    billings: state.BillingReducer.billings
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setBillings: (billings) => {
      dispatch({type: BillingActions.SET_BILLINGS, billings});
    },
    deleteBilling: (billingId) => {
      dispatch({type: BillingActions.DELETE_BILLING, billingId});
    }
  };
};

BillingContainer.propTypes = {
  billings: PropTypes.array,
  setBillings: PropTypes.func,
  deleteBilling: PropTypes.func,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BillingContainer));
