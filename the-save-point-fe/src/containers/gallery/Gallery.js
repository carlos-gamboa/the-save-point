import React, { Component } from 'react';
import './Gallery.scss';
import AliceCarousel from 'react-alice-carousel';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import GalleyItem from '../../components/card/gallery-item/GalleyItem';

class Gallery extends Component {

  handleOnDragStart = (event) => {
    event.preventDefault();
  }

  render() {

    const { title, products, ...otherProps } = this.props;
    const responsive = {
      0: { items: 1 },
      576: { items: 2 },
      768: { items: 3 },
      992: { items: 4 },
      1200: { items: 5 },
    };

    return (
      <div className='gallery'>
        <p className='gallery__title'>{title}</p>
        <AliceCarousel {...otherProps} mouseDragEnabled responsive={responsive} >
          {
            products.map((product) => {
              return (
                <GalleyItem 
                  key={product.id}
                  product={product}
                />
              );
            })
          }
        </AliceCarousel>
      </div>
    );
  }
}

Gallery.propTypes = {
  title: PropTypes.string,
  products: PropTypes.array
};

export default withRouter(Gallery);