import React, { Component } from 'react';
import Select from 'react-select';
import './ProductsContainer.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import Loading from '../../components/loading/Loading';
import ProductItem from '../../components/card/product-item/ProductItem';
import ProductHelper from '../../helpers/ProductHelper';
import ShoppingCartActions from '../../redux/shopping-cart/ShoppingCartActions';
import { ProductSearchHelper } from '../../helpers/ProductSearchHelper';
const queryString =  require('query-string');

class ProductsContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: 'Gaming',
      myRef: React.createRef(),
      products: [],
      search: '',
      brand: '',
      loading: true,
      sort: { value: 'releaseDate,desc', label: 'What\'s new?' },
      category: { value: '', label: 'All' },
      sortOptions: [
        { value: 'releaseDate,desc', label: 'What\'s new?' },
        { value: 'price,asc', label: 'Price low to high' },
        { value: 'price,desc', label: 'Price high to low' },
        { value: 'rating,desc', label: 'Rating' }       
      ],
      categoryOptions: [
        { value: '', label: 'All' },
        { value: 'console', label: 'Console' },
        { value: 'video_game', label: 'Video Games' },
        { value: 'accessory', label: 'Accessories' }
      ],
      pageCount: 0,
      page: 0
    };
  }

  componentDidMount = () => {
    const sort = this.getSelectFromQueryString('sort');
    const category = this.getSelectFromQueryString('category');
    const search = this.getSelectFromQueryString('search');
    const brand = this.getSelectFromQueryString('brand');
    const page = this.getSelectFromQueryString('page');
    this.setState({
      sort: sort,
      category: category,
      search: search,
      brand: brand,
      page: page,
      title: (brand) ? brand : 'Gaming'
    });
    this.getAllProducts(page, sort.value, search, category.value, brand);
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.location.search !== prevProps.location.search) {
      const sort = this.getSelectFromQueryString('sort');
      const category = this.getSelectFromQueryString('category');
      const search = this.getSelectFromQueryString('search');
      const brand = this.getSelectFromQueryString('brand');
      const page = this.getSelectFromQueryString('page');
      this.setState({
        sort: sort,
        category: category,
        search: search,
        brand: brand,
        page: page,
        title: (brand) ? brand : 'Gaming'
      });
      this.getAllProducts(page, sort.value, search, category.value, brand);
    }
  }

  getSelectFromQueryString = (field) => {
    const query = queryString.parse(this.props.location.search)[field];
    const productSearchHelper = new ProductSearchHelper();
    return productSearchHelper.getSelectFromQueryString(query, field);
  }

  getQueryStringFromSelect = (selectValue) => {
    const productSearchHelper = new ProductSearchHelper();
    return productSearchHelper.getQueryStringFromSelect(selectValue);
  }

  handleSortChange = (option) => {
    this.setState({
      sort: option,
      page: 0
    });
    this.props.history.push({ pathname: this.props.match.url, search: `?page=0&brand=${this.state.brand}&sort=${this.getQueryStringFromSelect(option.value)}&category=${this.getQueryStringFromSelect(this.state.category.value)}&search=${this.state.search}`});
  }

  handleCategoryChange = (option) => {
    this.setState({
      category: option,
      page: 0
    });
    this.props.history.push({ pathname: this.props.match.url, search: `?page=0&brand=${this.state.brand}&sort=${this.getQueryStringFromSelect(this.state.sort.value)}&category=${this.getQueryStringFromSelect(option.value)}&search=${this.state.search}`});
  }

  handlePageChange = (page) => {
    this.setState({
      page: page.selected
    });
    this.props.history.push({ pathname: this.props.match.url, search: `?page=${page.selected}&brand=${this.state.brand}&sort=${this.getQueryStringFromSelect(this.state.sort.value)}&category=${this.getQueryStringFromSelect(this.state.category.value)}&search=${this.state.search}`});
    this.state.myRef.current.scrollIntoView({behavior: 'smooth'});
  }

  handleSearchChange = (event) => {
    event.preventDefault();
    this.setState({
      search: event.target.value,
    });
  }
    
  handleSearch = (event) => {
    event.preventDefault();
    this.setState({
      page: 0
    });

    this.props.history.push({ pathname: this.props.match.url, search: `?page=0&brand=${this.state.brand}&sort=${this.getQueryStringFromSelect(this.state.sort.value)}&category=${this.getQueryStringFromSelect(this.state.category.value)}&search=${this.state.search}`});
  }

  getAllProducts = (pageNumber, sort, search, category, brand) => {
    const productsHelper = new ProductHelper();
    productsHelper.getAllProducts(pageNumber, sort, search, category, brand)
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          loading: false,
          products: data.data,
          page: data.pagination.pageNumber,
          pageCount: data.pagination.totalPages
        });
      })
      .catch(() => {
        this.props.history.replace('/error');
      });
  }

  onAddToShoppingCart = (id) => {
    const productsHelper = new ProductHelper();
    productsHelper.addToShoppingCart({quantity: 1, product: {id: id}})
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.onAddToShoppingCart(data.data);
      });
  }

  render() {
    const { myRef, page, pageCount, brand, title, search, products, loading, sort, category, sortOptions, categoryOptions } = this.state;

    const customSelectStyles = {
      option: (provided, state) => ({
        ...provided,
        backgroundColor: state.isSelected ? '#D01345' : '#FFF',
        color: state.isSelected ? '#FFF' : '#D01345',
        '&:hover': {backgroundColor: state.isSelected ? '#850D2C' : '#F2BECC99'}
      }),
      control: (provided, state) => ({
        ...provided,
        backgroundColor: '#EEE',
        '&:hover': {borderColor: '#D01345'},
        borderColor: state.isFocused ? '#D01345' : '#BBB',
        boxShadow: 'none'
      }),
      singleValue: (provided, state) => ({
        ...provided,
        opacity: state.isDisabled ? 0.5 : 1,
        transition: 'opacity 300ms'
      }),
      dropdownIndicator: (provided) => ({
        ...provided,
        color: '#999',
        '&:hover': {color: '#D01345'}
      })
    };

    return (
      <main className='products-container'>
        <div className='products-container__container'>
          <h1 className='products-container__heading'>{title}</h1>
          <form className='products-container__search'>
            <input name='search'
              aria-label='Search Products'
              value={search}
              placeholder='Search Products by Name'
              onChange={this.handleSearchChange} type='text' className='products-container__input'/>
            <button type='submit' className='products-container__button' onClick={this.handleSearch}>Search</button>
          </form>
          <section className='products-container__breadcrumbs'>
            <Link to='/product' className='products-container__breadcrumb'>Products</Link>
            
            {
              brand && brand !== '' ?
                <React.Fragment>
                  <span className='products-container__breadcrumb-separator'>&gt;</span>
                  <Link to={`/product?brand=${brand}`} className='products-container__breadcrumb'>{brand}</Link>
                </React.Fragment>
                : null
            }
            
          </section>
          <section className='products-container__filters' ref={myRef}>
            <div className='products-container__filter'>
              <label className='products-container__label' htmlFor='sort'>Sort</label>
              <Select styles={customSelectStyles} className='products-container__select' classNamePrefix='select' name='sort' id='sort' options={sortOptions} value={sort} onChange={(value) => this.handleSortChange(value)} />
            </div>
            <div className='products-container__filter'>
              <label className='products-container__label' htmlFor='category'>Category</label>
              <Select styles={customSelectStyles} className='products-container__select' classNamePrefix='select' name='category' id='category' options={categoryOptions} value={category} onChange={(value) => this.handleCategoryChange(value)} />
            </div>
          </section>
          {(loading) ?
            <section className='products-container__loading-wrapper'>
              <Loading></Loading> 
            </section>
            :
            <React.Fragment>
              <section className='products-container__wrapper'>
                { products.map((product) => {
                  return (
                    <ProductItem 
                      key={product.id}
                      product={product} 
                      onAddToShoppingCart={this.onAddToShoppingCart.bind(this)} 
                      match={this.props.match}
                    />
                  );
                })}
              </section>
              <div className='products-container__pagination'>
                <ReactPaginate
                  previousLabel={'<'}
                  nextLabel={'>'}
                  breakLabel={'...'}
                  breakClassName={'pagination__break-me'}
                  pageCount={pageCount}
                  forcePage={page}
                  initialPage={page}
                  marginPagesDisplayed={0}
                  pageRangeDisplayed={2}
                  onPageChange={this.handlePageChange}
                  containerClassName={'pagination'}
                  subContainerClassName={'pagination__page pagination'}
                  activeClassName={'pagination--active'}
                /></div>
            </React.Fragment>
          }
        </div>
      </main>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToShoppingCart: (item) => {
      dispatch({type: ShoppingCartActions.ADD_TO_CART, item});
    }
  };
};

ProductsContainer.propTypes = {
  onAddToShoppingCart: PropTypes.func,
  location: PropTypes.object,
  history: PropTypes.object,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductsContainer));
