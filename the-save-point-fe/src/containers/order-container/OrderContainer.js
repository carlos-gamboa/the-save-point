import React, { Component } from 'react';
import './OrderContainer.scss';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import OrderItem from '../../components/card/order-item/OrderItem';
import ProductHelper from '../../helpers/ProductHelper';
import Loading from '../../components/loading/Loading';

class OrderContainer extends Component {

  constructor() {
    super();

    this.state = {
      orders: [],
      loading: true
    };
  }

  componentDidMount = () => {
    const productHelper = new ProductHelper();
    productHelper.getOrders()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          orders: data.data,
          loading: false
        });
      });
  }

  render = () => {
    const { orders, loading } = this.state;

    const content = (loading) ?
      <div className='order-container__loading-wrapper'>
        <Loading></Loading> 
      </div>
      :
      <div className='order-container__wrapper'>
        { orders.map((order) => {
          return (
            <OrderItem 
              key={order.id}
              order={order}
            />
          );
        })}
      </div>;

    return (
      <div className='order-container'>
        <h2 className='order-container__heading'>Your Orders</h2>
        { content }
      </div>
    );
  }
}

OrderContainer.propTypes = {
  match: PropTypes.object
};

export default withRouter(OrderContainer);
