import React, { Component } from 'react';
import './PaymentContainer.scss';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SimpleCard from '../../components/card/simple-card/SimpleCard';
import PaymentHelper from '../../helpers/PaymentHelper';
import PaymentActions from '../../redux/payment/PaymentActions';
import Loading from '../../components/loading/Loading';

class PaymentContainer extends Component {

  constructor() {
    super();

    this.state = {
      loading: true
    };
  }

  componentDidMount = () => {
    const paymentHelper = new PaymentHelper();
    paymentHelper.getAllPayments()
      .then((response) => {
        if (response.status > 400) {
          throw response;
        }
        return response.json();
      })
      .then((data) => {
        this.props.setPayments(data.data);
        this.setState({
          loading: false
        });
      })
      .catch(() => {
        this.setState({
          loading: false
        });
      });
  }

  onDelete = (id) => {
    const paymentHelper = new PaymentHelper();
    paymentHelper.deletePayment(id)
      .then((response) => {
        if (response.status !== 200) {
          throw response;
        }
        this.props.deletePayment(id);
      });
  }

  formatCardNumber = (cardNumber) => {
    return `(${cardNumber.substring(cardNumber.length - 4, cardNumber.length)})`;
  };

  render = () => {
    const { loading } = this.state;
    const { payments } = this.props;

    const content = (loading) ?
      <div className='payment-container__loading-wrapper'>
        <Loading></Loading> 
      </div>
      :
      <div className='payment-container__wrapper'>
        { payments.map((payment) => {
          return (
            <SimpleCard 
              key={payment.id}
              itemId={payment.id} 
              onDelete={this.onDelete.bind(this)} 
              match={this.props.match}
            >
              <div className='simple-card__text-wrapper'>
                <i className='material-icons simple-card__icon'>person</i>
                <p className='simple-card__text'>{payment.cardName}</p>
              </div>
              <div className='simple-card__text-wrapper'>
                <i className='material-icons simple-card__icon'>credit_card</i>
                <p className='simple-card__text'>{this.formatCardNumber(payment.cardNumber)}</p>
              </div>
              <div className='simple-card__text-wrapper'>
                <p className='simple-card__text'>Expiration Date: {payment.expirationDate}</p>
              </div>
            </SimpleCard>
          );
        })}
      </div>;

    return (
      <div className='payment-container'>
        <div className='payment-container__top'>
          <h2 className='payment-container__heading'>Your Payment Methods</h2>
          <Link to={this.props.match.url + '/new'} className='payment-container__button'>Add new Payment method</Link>
        </div>
        { content }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    payments: state.PaymentReducer.payments
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPayments: (payments) => {
      dispatch({type: PaymentActions.SET_CARDS, payments});
    },
    deletePayment: (paymentId) => {
      dispatch({type: PaymentActions.DELETE_CARD, paymentId});
    }
  };
};

PaymentContainer.propTypes = {
  payments: PropTypes.array,
  setPayments: PropTypes.func,
  deletePayment: PropTypes.func,
  match: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PaymentContainer));
