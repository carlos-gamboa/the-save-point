export default class DateHelper {

  constructor() {
    this.m_names = ['January', 'February', 'March', 
      'April', 'May', 'June', 'July', 'August', 'September', 
      'October', 'November', 'December'];
  
    this.d_names = ['Sunday','Monday', 'Tuesday', 'Wednesday', 
      'Thursday', 'Friday', 'Saturday'];
  }

  /**
   * Gets the Name of the Day of the Week.
   *
   * @param {number} day Number of the day.
   * @returns String with the name of the day.
   */
  getDayName = (day) => {
    return this.d_names[day];
  }

  /**
   * Gets the Name of the Month.
   *
   * @param {number} month Number of the month
   * @returns String with the name of the month.
   */
  getMonthName = (month) => {
    return this.m_names[month];
  }


  /**
   * Checks if 2 dates have the same Day, Month and Year.
   *
   * @param {Date} firstDate
   * @param {Date} secondDate
   * @returns
   */
  areSameDate = (firstDate, secondDate) => {
    return (
      firstDate.getFullYear() === secondDate.getFullYear() &&
      firstDate.getMonth() === secondDate.getMonth() &&
      firstDate.getDate() === secondDate.getDate()
    );
  }

  /**
   * Creates a Date from a SQL Date
   *
   * @param {*} date SQL Date
   * @returns Date
   */
  getDateFromSQLDate = (date) => {
    const dateParts = date.split('-');
    return new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
  }
}