export class ProductSearchHelper {
  getSelectFromQueryString = (query, field) => {
    let selectValue;
    if (field === 'sort') {
      if (query === 'newest') {
        selectValue = { value: 'releaseDate,desc', label: 'What\'s new?' };
      }
      else if (query === 'price low') {
        selectValue = { value: 'price,asc', label: 'Price low to high' };
      }
      else if (query === 'price high') {
        selectValue = { value: 'price,desc', label: 'Price high to low' };
      }
      else if (query === 'rating') {
        selectValue = { value: 'rating,desc', label: 'Rating' };
      }
      else if (!query) {
        selectValue = { value: 'releaseDate,desc', label: 'What\'s new?' };
      }
      else {
        selectValue = { value: 'releaseDate,desc', label: 'What\'s new?' };
        this.props.history.replace('/');
      }
    } 
    else if (field === 'category') {
      if (query === 'all') {
        selectValue = { value: '', label: 'All' };
      }
      else if (query === 'console') {
        selectValue = { value: 'console', label: 'Console' };
      }
      else if (query === 'video_game') {
        selectValue = { value: 'video_game', label: 'Video Games' };
      }
      else if (query === 'accessory') {
        selectValue = { value: 'accessory', label: 'Accessories' };
      }
      else if (!query) {
        selectValue = { value: '', label: 'All' };
      }
      else {
        selectValue = { value: '', label: 'All' };
        this.props.history.replace('/');
      }
    }
    else if (field === 'page') {
      if (query !== undefined) {
        selectValue = parseInt(query);
      }
      else {
        selectValue = 0;
      }
    }
    else {
      if (query !== undefined) {
        selectValue = query;
      }
      else {
        selectValue = '';
      }
    }
    return selectValue;
  }

  getQueryStringFromSelect = (selectValue) => {
    let queryString;
    if (selectValue === 'releaseDate,desc') {
      queryString = 'newest';
    }
    else if (selectValue === 'price,asc') {
      queryString = 'price+low';
    }
    else if (selectValue === 'price,desc') {
      queryString = 'price+high';
    }
    else if (selectValue === 'rating,desc') {
      queryString = 'rating';
    }
    else if (selectValue === '') {
      queryString = 'all';
    }
    else if (selectValue === 'console') {
      queryString = 'console';
    }
    else if (selectValue === 'video_game') {
      queryString = 'video_game';
    }
    else if (selectValue === 'accessory') {
      queryString = 'accessory';
    }
    return queryString;
  }
}