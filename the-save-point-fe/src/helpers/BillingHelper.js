import Environment from '../env';

export default class BillingHelper {

  /**
   * Generates the headers for the requests
   *
   * @returns Headers
   */
  getCustomHeaders = () => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth-token', localStorage.getItem('token'));
    return headers;
  }

  /**
   * Call to the API to get all the Billings related to a User.
   *
   * @returns Promise with the API response.
   */
  getAllBillings = () => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/billing', config);
  }


  /**
   * Call to the API to get a Billing based on it's Id.
   *
   * @param {number} id Id of the billing.
   * @returns Promise with the API response.
   */
  getBilling = (id) => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/billing/' + id, config);
  }

  /**
   * Call to the API to delete a Billing based on it's Id.
   *
   * @param {number} id Id of the billing.
   * @returns Promise with the API response.
   */
  deleteBilling = (id) => {
    const config = { 
      method: 'DELETE',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/billing/' + id, config);
  }

  /**
   * Call to the API to create a new Billing.
   *
   * @param {Billing} billing Data of the billing.
   * @returns Promise with the API response.
   */
  createBilling = (billing) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(),
      body: JSON.stringify(billing)
    };

    return fetch(Environment.API_BASE_URL + '/billing', config);
  }

  /**
   * Call to the API to update a Billing.
   *
   * @param {number} id Id of the Billing.
   * @param {Billing} billing Data of the billing.
   * @returns Promise with the API response.
   */
  updateBilling = (id, billing) => {
    const config = { 
      method: 'PUT',
      headers: this.getCustomHeaders(),
      body: JSON.stringify(billing)
    };

    return fetch(Environment.API_BASE_URL + '/billing/' + id, config);
  }
}