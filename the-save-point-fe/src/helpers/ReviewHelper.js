import Environment from '../env';

export default class BillingHelper {

  /**
   * Generates the headers for the requests
   *
   * @returns Headers
   */
  getCustomHeaders = () => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth-token', localStorage.getItem('token'));
    return headers;
  }

  /**
   * Call to the API to get the reviews related to a Product.
   *
   * @param {object} data Data containing the Product id.
   * @returns Promise with the API response.
   */
  getProductReviews = (data) => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(),
      body: JSON.stringify(data)
    };

    return fetch(Environment.API_BASE_URL + '/review', config);
  }

  /**
   * Call to the API to delete a Review based on it's Id.
   *
   * @param {number} id Id of the review.
   * @returns Promise with the API response.
   */
  deleteReview = (id) => {
    const config = { 
      method: 'DELETE',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/review/' + id, config);
  }

  /**
   * Call to the API to create a new Review.
   *
   * @param {object} review Data of the review.
   * @returns Promise with the API response.
   */
  addReview = (review) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(),
      body: JSON.stringify(review)
    };

    return fetch(Environment.API_BASE_URL + '/review', config);
  }

}