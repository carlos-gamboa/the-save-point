import Environment from '../env';

export default class ProductHelper {

  /**
   * Generates the headers for the requests
   *
   * @returns Headers
   */
  getCustomHeaders = (addAuth) => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    if (addAuth) {
      headers.append('auth-token', localStorage.getItem('token'));
    }
    return headers;
  }

  /**
   * Call to the API to get all the Products sorted.
   *
   * @param {number} page Page to sort.
   * @param {string} sort Sort parameter.
   * @param {string} search String that will be contained in the name.
   * @param {string} category Product's category.
   * @param {string} brand Product's brand.
   * @returns Promise with the API response.
   */
  getAllProducts = (page, sort, search, category, brand) => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(false)
    };

    return fetch(`${Environment.API_BASE_URL}/product?page=${page}&size=6&sort=${sort}&search=${search}&category=${category}&brand=${brand}`, config);
  }

  /**
   * Call to the API to get a Product based on a category and id.
   *
   * @param {string} category Category of the Product.
   * @param {number} id Id of the Product.
   * @returns Promise with the API response.
   */
  getProduct = (category, id) => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(false)
    };

    return fetch(`${Environment.API_BASE_URL}/product/${category}/${id}`, config);
  }

  /**
   * Adds an Item to the Shopping Cart.
   *
   * @param {object} item Item to be added.
   * @returns Promise with the API response.
   */
  addToShoppingCart = (item) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(true),
      body: JSON.stringify(item)
    };

    return fetch(`${Environment.API_BASE_URL}/shopping-cart`, config);
  }

  /**
   * Call to the API to delete an item from the shopping cart.
   *
   * @param {number} itemId Id of the item to delete.
   * @returns Promise with the API response.
   */
  removeFromShoppingCart = (itemId) => {
    const config = { 
      method: 'DELETE',
      headers: this.getCustomHeaders(true)
    };

    return fetch(`${Environment.API_BASE_URL}/shopping-cart/${itemId}`, config);
  }

  /**
   * Call to the API to update a shopping cart item.
   *
   * @param {number} itemId Id of the item to update.
   * @param {object} item Item to update.
   * @returns Promise with the API response.
   */
  updateShoppingCart = (item, itemId) => {
    const config = { 
      method: 'PUT',
      headers: this.getCustomHeaders(true),
      body: JSON.stringify(item)
    };

    return fetch(`${Environment.API_BASE_URL}/shopping-cart/${itemId}`, config);
  }

  
  /**
   * Call to the API to get all the products in the logged user's shopping cart.
   *
   * @returns Promise with the API response.
   */
  getShoppingCart = () => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(true)
    };

    return fetch(`${Environment.API_BASE_URL}/shopping-cart`, config);
  }

  
  /**
   * Call to the API to checkout the shopping cart.
   *
   * @param {array} data All the items to checkout.
   * @param {number} billingId Id of the Billing address
   * @returns Promise with the API response.
   */
  checkout = (data, billingId) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(true),
      body: JSON.stringify({ status: 'pending', shoppingCart: data,
        billing: { id: billingId } })
    };

    return fetch(`${Environment.API_BASE_URL}/order`, config);
  }


  /**
   * Call to the API to get an order
   *
   * @param {number} orderId
   * @returns Promise with the API response.
   */
  getOrder = (orderId) => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(true)
    };

    return fetch(`${Environment.API_BASE_URL}/order/${orderId}`, config);
  }

  /**
   * Call to the API to update an Order.
   *
   * @param {number} orderId Id of the order to update.
   * @param {object} order Order to update.
   * @returns Promise with the API response.
   */
  updateOrder = (order, orderId) => {
    const config = { 
      method: 'PUT',
      headers: this.getCustomHeaders(true),
      body: JSON.stringify(order)
    };

    return fetch(`${Environment.API_BASE_URL}/order/${orderId}`, config);
  }

  /**
   * Call to the API to get the user's orders
   *
   * @returns Promise with the API response.
   */
  getOrders = () => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(true)
    };

    return fetch(`${Environment.API_BASE_URL}/order`, config);
  }

}