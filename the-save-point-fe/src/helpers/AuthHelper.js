import Environment from '../env';

export default class AuthHelper {

  /**
   * Generates the headers for the requests
   *
   * @returns Headers
   */
  getCustomHeaders = (addAuth) => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    if (addAuth) {
      headers.append('auth-token', localStorage.getItem('token'));
    }
    return headers;
  }

  /**
   * Call to the API to register a new user.
   *
   * @param {Object} data Body of the request.
   * @returns Promise with the API response.
   */
  register = (data) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(false),
      body: JSON.stringify(data)
    };

    return fetch(Environment.API_BASE_URL + '/user', config);
  }


  /**
   * Call to the API to authenticate the user.
   *
   * @param {Object} data Body of the request.
   * @returns Promise with the API response.
   */
  login = (data) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(false),
      body: JSON.stringify(data)
    };

    return fetch(Environment.API_BASE_URL + '/user/login', config);
  }

  /**
   * Call to the API to renew the user's token.
   *
   * @returns Promise with the API response.
   */
  renew = () => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders(true)
    };

    return fetch(Environment.API_BASE_URL + '/user/renew', config);
  }
}