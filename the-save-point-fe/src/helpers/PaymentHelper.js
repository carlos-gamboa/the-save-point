import Environment from '../env';

export default class PaymentHelper {

  /**
   * Generates the headers for the requests
   *
   * @returns Headers
   */
  getCustomHeaders = () => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth-token', localStorage.getItem('token'));
    return headers;
  }

  /**
   * Call to the API to get all the Payments related to a User.
   *
   * @returns Promise with the API response.
   */
  getAllPayments = () => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/card', config);
  }


  /**
   * Call to the API to get a Payment based on it's Id.
   *
   * @param {number} id Id of the Payment.
   * @returns Promise with the API response.
   */
  getPayment = (id) => {
    const config = { 
      method: 'GET',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/card/' + id, config);
  }

  /**
   * Call to the API to delete a Payment based on it's Id.
   *
   * @param {number} id Id of the Payment.
   * @returns Promise with the API response.
   */
  deletePayment = (id) => {
    const config = { 
      method: 'DELETE',
      headers: this.getCustomHeaders()
    };

    return fetch(Environment.API_BASE_URL + '/card/' + id, config);
  }

  /**
   * Call to the API to create a new Payment.
   *
   * @param {Payment} payment Data of the card.
   * @returns Promise with the API response.
   */
  createPayment = (payment) => {
    const config = { 
      method: 'POST',
      headers: this.getCustomHeaders(),
      body: JSON.stringify(payment)
    };

    return fetch(Environment.API_BASE_URL + '/card', config);
  }

  /**
   * Call to the API to update a Payment.
   *
   * @param {number} id Id of the Payment.
   * @param {Payment} payment Data of the card.
   * @returns Promise with the API response.
   */
  updatePayment = (id, payment) => {
    const config = { 
      method: 'PUT',
      headers: this.getCustomHeaders(),
      body: JSON.stringify(payment)
    };

    return fetch(Environment.API_BASE_URL + '/card/' + id, config);
  }
}