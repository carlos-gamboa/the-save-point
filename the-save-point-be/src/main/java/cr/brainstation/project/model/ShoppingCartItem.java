package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.ShoppingCartItemDTO;

public class ShoppingCartItem {

    private Long id;
    private Integer quantity;
    private Product product;

    public ShoppingCartItem() {
    }

    public ShoppingCartItem(ShoppingCartItemDTO shoppingCartItemDTO) {
        this.id = shoppingCartItemDTO.getId();
        this.quantity = shoppingCartItemDTO.getQuantity();
        this.product = new Product(shoppingCartItemDTO.getProduct());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
