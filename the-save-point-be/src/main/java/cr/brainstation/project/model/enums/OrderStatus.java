package cr.brainstation.project.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum OrderStatus {
    CANCELLED("cancelled"),
    PENDING("pending"),
    ACCEPTED("accepted");

    private String value;

    OrderStatus(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() { return this.value; }

    @JsonCreator
    public static OrderStatus create(String val) {
        OrderStatus[] orderStatuses = OrderStatus.values();
        for (OrderStatus orderStatus : orderStatuses) {
            if (orderStatus.getValue().equals(val)) {
                return orderStatus;
            }
        }
        return PENDING;
    }
}
