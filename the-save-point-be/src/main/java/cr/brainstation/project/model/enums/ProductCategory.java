package cr.brainstation.project.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductCategory {
    CONSOLE("console"),
    VIDEO_GAME("video_game"),
    ACCESSORY("accessory"),
    NONE("");

    private String value;

    ProductCategory(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() { return this.value; }

    @JsonCreator
    public static ProductCategory create(String val) {
        ProductCategory[] categories = ProductCategory.values();
        for (ProductCategory category : categories) {
            if (category.getValue().equals(val)) {
                return category;
            }
        }
        return NONE;
    }
}
