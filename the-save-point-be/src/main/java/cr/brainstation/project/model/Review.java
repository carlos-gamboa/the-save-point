package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.ReviewDTO;

import java.time.LocalDateTime;

public class Review {

    private Long id;

    private String title;

    private String comment;

    private Float rating;

    private Integer likes;

    private Integer dislikes;

    private LocalDateTime publicationDate;

    private User user;

    private Product product;

    public Review() {
    }

    public Review(ReviewDTO reviewDTO) {
        this.id = reviewDTO.getId();
        this.title = reviewDTO.getTitle();
        this.comment = reviewDTO.getComment();
        this.rating = reviewDTO.getRating();
        this.likes = reviewDTO.getLikes();
        this.dislikes = reviewDTO.getDislikes();
        this.publicationDate = reviewDTO.getPublicationDate();
        this.user = new User(reviewDTO.getUser());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public LocalDateTime getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(LocalDateTime publicationDate) {
        this.publicationDate = publicationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
