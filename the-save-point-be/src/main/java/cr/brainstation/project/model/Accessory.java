package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.AccessoryDTO;

public class Accessory extends Product {
    private Console console;

    public Accessory() {
    }

    public Accessory(AccessoryDTO accessoryDTO) {
        super(accessoryDTO);
        if (accessoryDTO.getConsole() != null){
            this.console = new Console(accessoryDTO.getConsole());
        }
    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }
}
