package cr.brainstation.project.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Genre {
    ACTION("action"),
    ADVENTURE("adventure"),
    ROLEPLAY("roleplay"),
    SIMULATION("simulation"),
    STRATEGY("strategy"),
    SPORTS("sports"),
    OTHER("other");

    private String value;

    Genre(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() { return this.value; }

    @JsonCreator
    public static Genre create(String val) {
        Genre[] genres = Genre.values();
        for (Genre genre : genres) {
            if (genre.getValue().equals(val)) {
                return genre;
            }
        }
        return OTHER;
    }
}
