package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.VideoGameDTO;
import cr.brainstation.project.model.enums.Genre;

import java.util.Set;
import java.util.stream.Collectors;

public class VideoGame extends Product {

    private Product product;

    private String synopsis;

    private String developer;

    private String publisher;

    private Genre genre;

    private Set<Console> consoles;

    public VideoGame() {
    }

    public VideoGame(VideoGameDTO videoGameDTO) {
        super(videoGameDTO);
        this.synopsis = videoGameDTO.getSynopsis();
        this.developer = videoGameDTO.getDeveloper();
        this.publisher = videoGameDTO.getPublisher();
        this.genre = videoGameDTO.getGenre();
        this.consoles = videoGameDTO.getConsoles().stream()
                .map(Console::new)
                .collect(Collectors.toSet());
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Set<Console> getConsoles() {
        return consoles;
    }

    public void setConsoles(Set<Console> consoles) {
        this.consoles = consoles;
    }
}
