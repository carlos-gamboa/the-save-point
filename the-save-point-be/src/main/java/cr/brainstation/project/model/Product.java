package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.ProductDTO;
import cr.brainstation.project.core.dto.VideoGameDTO;
import cr.brainstation.project.model.enums.Brand;
import cr.brainstation.project.model.enums.ProductCategory;

import java.time.LocalDate;
import java.util.List;

public class Product {

    protected Long id;

    protected String name;

    protected Float price;

    protected Brand brand;

    protected Integer leftInStock;

    protected LocalDate releaseDate;

    protected String description;

    protected String productPictureLink;

    protected Float rating;

    protected ProductCategory category;

    protected List<Review> reviews;

    public Product() {
    }

    public Product(ProductDTO productDTO) {
        this.id = productDTO.getId();
        this.name = productDTO.getName();
        this.price = productDTO.getPrice();
        this.brand = productDTO.getBrand();
        this.leftInStock = productDTO.getLeftInStock();
        this.releaseDate = productDTO.getReleaseDate();
        this.description = productDTO.getDescription();
        this.productPictureLink = productDTO.getProductPictureLink();
        this.rating = (productDTO.getRating() == null) ? 0 : productDTO.getRating();
        this.category = productDTO.getCategory();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Integer getLeftInStock() {
        return leftInStock;
    }

    public void setLeftInStock(Integer leftInStock) {
        this.leftInStock = leftInStock;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductPictureLink() {
        return productPictureLink;
    }

    public void setProductPictureLink(String productPictureLink) {
        this.productPictureLink = productPictureLink;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
}
