package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.ReviewService;
import cr.brainstation.project.model.Review;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @PostMapping("/review")
    public ResponseEntity createReview(@RequestAttribute(name = "userId") String userId,
                                        @RequestBody Review review) {
        try {
            Review newReview = this.reviewService.addReview(userId, review);
            return new ResponseEntity<>(new SuccessResponse(newReview), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/review/{reviewId}")
    public ResponseEntity updateReview(@RequestAttribute(name = "userId") String userId,
                                        @PathVariable(name = "reviewId") String reviewId,
                                        @RequestBody Review review) {
        try {
            Review updatedReview = this.reviewService.updateReview(userId, reviewId, review);
            return new ResponseEntity<>(new SuccessResponse(updatedReview), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/review/{reviewId}")
    public ResponseEntity getReview(@PathVariable(name = "reviewId") String reviewId) {
        try {
            Review review = this.reviewService.getReview(reviewId);
            return new ResponseEntity<>(new SuccessResponse(review), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/review/{reviewId}")
    public ResponseEntity deleteReview(@PathVariable(name = "reviewId") String reviewId) {
        try {
            this.reviewService.deleteReview(reviewId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

}
