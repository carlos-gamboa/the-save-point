package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.OrderService;
import cr.brainstation.project.model.Order;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.exception.NotEnoughProductException;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/order")
    public ResponseEntity createOrder(@RequestAttribute(name = "userId") String userId,
                                      @RequestBody Order order) {
        try {
            Order newOrder = this.orderService.createOrder(userId, order);
            return new ResponseEntity<>(new SuccessResponse(newOrder), HttpStatus.OK);
        }
        catch (NotEnoughProductException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/order")
    public ResponseEntity getOrders(@RequestAttribute(name = "userId") String userId) {
        try {
            List<Order> orders = this.orderService.getOrders(userId);
            return new ResponseEntity<>(new SuccessResponse(orders), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/order/{orderId}")
    public ResponseEntity updateBilling(@RequestAttribute(name = "userId") String userId,
                                        @PathVariable(name = "orderId") String orderId,
                                        @RequestBody Order order) {
        try {
            Order updatedOrder = this.orderService.updateOrder(userId, orderId, order);
            return new ResponseEntity<>(new SuccessResponse(updatedOrder), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/order/{orderId}")
    public ResponseEntity getOrder(@PathVariable(name = "orderId") String orderId) {
        try {
            Order newOrder = this.orderService.getOrder(new Long(orderId));
            return new ResponseEntity<>(new SuccessResponse(newOrder), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

}
