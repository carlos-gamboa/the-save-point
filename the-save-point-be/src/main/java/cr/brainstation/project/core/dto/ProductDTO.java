package cr.brainstation.project.core.dto;

import cr.brainstation.project.core.dto.converter.BrandConverter;
import cr.brainstation.project.model.Product;
import cr.brainstation.project.model.enums.Brand;
import cr.brainstation.project.model.enums.ProductCategory;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 * Super class that represents the products that can be sold in the application.
 */
@Entity
@Table(name = "products")
@Inheritance(strategy = InheritanceType.JOINED)
public class ProductDTO extends BaseEntity {
    /**
     * Name.
     */
    @Column(name = "product_name", nullable = false)
    protected String name;

    /**
     * Price.
     */
    @Column(name = "price", nullable = false)
    protected Float price;

    /**
     * Product's Brand.
     */
    @Column(name = "brand", nullable = false)
    @Convert(converter = BrandConverter.class)
    protected Brand brand;

    /**
     * Number of products left in stock.
     */
    @Column(name = "left_stock", nullable = false, columnDefinition = "int default 1")
    protected Integer leftInStock;

    /**
     * Release date.
     */
    @Column(name = "release_date", nullable = false)
    protected LocalDate releaseDate;

    /**
     * Text with the Product's description.
     */
    @Column(name = "description", columnDefinition = "TEXT")
    protected String description;

    /**
     * Link to the Product's picture.
     */
    @Column(name = "product_picture_link")
    protected String productPictureLink;

    /**
     * Product's category.
     */
    @Column(name = "category")
    protected ProductCategory category;

    /**
     * Product's review average
     */
    @Formula("(select avg(r.rating) from reviews r where r.product_id = id)")
    protected Float rating;

    /**
     * Set of the Shopping Carts the Product is in.
     */
    @OneToMany(mappedBy="product", fetch = FetchType.LAZY)
    protected Set<ShoppingCartItemDTO> shoppingCart;

    /**
     * Set of the Reviews for the Product.
     */
    @OneToMany(mappedBy="product", fetch = FetchType.LAZY)
    protected Set<ReviewDTO> reviews;

    public ProductDTO() {
    }

    public ProductDTO(Product product) {
        this.id = product.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Integer getLeftInStock() {
        return leftInStock;
    }

    public void setLeftInStock(Integer leftInStock) {
        this.leftInStock = leftInStock;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductPictureLink() {
        return productPictureLink;
    }

    public void setProductPictureLink(String productPictureLink) {
        this.productPictureLink = productPictureLink;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Set<ShoppingCartItemDTO> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(Set<ShoppingCartItemDTO> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Set<ReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(Set<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

}
