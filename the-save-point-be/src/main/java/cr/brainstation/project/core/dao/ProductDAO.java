package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.ProductDTO;
import cr.brainstation.project.model.enums.Brand;
import cr.brainstation.project.model.enums.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDAO extends JpaRepository<ProductDTO, Long> {

    Page<ProductDTO> findAllByNameIgnoreCaseContaining(Pageable pageable, String name);

    Page<ProductDTO> findAllByCategoryAndBrandAndNameIgnoreCaseContaining(Pageable pageable, ProductCategory category,
                                                                                    Brand brand, String name);

    Page<ProductDTO> findAllByCategoryAndNameIgnoreCaseContaining(Pageable pageable, ProductCategory category, String name);

    Page<ProductDTO> findAllByBrandAndNameIgnoreCaseContaining(Pageable pageable, Brand brand, String name);

    Page<ProductDTO> findAllByCategory(Pageable pageable, ProductCategory category);

}
