package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.BillingDTO;
import cr.brainstation.project.core.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface BillingDAO extends JpaRepository<BillingDTO, Long> {
    Set<BillingDTO> findAllByUserAndIsActive(UserDTO user, Boolean isActive);
}
