package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<UserDTO, Long> {

    UserDTO findByUsername(String username);

}
