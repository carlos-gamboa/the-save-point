package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.OrderDAO;
import cr.brainstation.project.core.dao.ProductDAO;
import cr.brainstation.project.core.dao.ShoppingCartDAO;
import cr.brainstation.project.core.dao.UserDAO;
import cr.brainstation.project.core.dto.OrderDTO;
import cr.brainstation.project.core.dto.ProductDTO;
import cr.brainstation.project.core.dto.ShoppingCartItemDTO;
import cr.brainstation.project.core.dto.UserDTO;
import cr.brainstation.project.core.service.ShoppingCartService;
import cr.brainstation.project.model.Order;
import cr.brainstation.project.model.ShoppingCartItem;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.exception.NotEnoughProductException;
import cr.brainstation.project.support.exception.UnauthorizedException;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Autowired
    private ShoppingCartDAO shoppingCartDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public ShoppingCartItem addToShoppingCart(ShoppingCartItem shoppingCart, String userId) {
        UserDTO user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        ProductDTO productDTO = this.productDAO.findById(shoppingCart.getProduct().getId()).orElse(null);

        if (productDTO == null) {
            throw new EntityNotFoundException("product", "id");
        }

        ShoppingCartItemDTO newShoppingCart = this.shoppingCartDAO.findByProductAndUserAndOrderNull(productDTO, user);

        if (newShoppingCart == null) {
            newShoppingCart = new ShoppingCartItemDTO(shoppingCart);
            newShoppingCart.setUser(user);

            newShoppingCart.setProduct(productDTO);
        }
        else {
            newShoppingCart.setQuantity(newShoppingCart.getQuantity() + 1);
        }

        newShoppingCart = this.shoppingCartDAO.save(newShoppingCart);
        return new ShoppingCartItem(newShoppingCart);

    }

    @Override
    @Transactional
    public ShoppingCartItem updateShoppingCartItem(String shoppingCartId, ShoppingCartItem shoppingCart, String userId) {
        UserDTO user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        ShoppingCartItemDTO newShoppingCart = this.shoppingCartDAO.findById(Long.parseLong(shoppingCartId)).orElse(null);

        if (newShoppingCart == null) {
            throw new EntityNotFoundException("shopping cart", "id");
        }
        if (user != newShoppingCart.getUser()) {
            throw new UnauthorizedException();
        }

        newShoppingCart.setQuantity(shoppingCart.getQuantity());

        newShoppingCart = this.shoppingCartDAO.save(newShoppingCart);
        return new ShoppingCartItem(newShoppingCart);
    }

    @Override
    @Transactional
    public List<ShoppingCartItem> getShoppingCart(String userId) {
        UserDTO user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        return this.shoppingCartDAO.findAllByUserAndOrderIsNull(user).stream()
                .map(ShoppingCartItem::new)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void removeFromShoppingCart(String shoppingCartId, String userId) {
        UserDTO user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        ShoppingCartItemDTO shoppingCart = this.shoppingCartDAO.findById(Long.parseLong(shoppingCartId)).orElse(null);

        if (shoppingCart == null) {
            throw new EntityNotFoundException("shopping cart", "id");
        }
        if (user != shoppingCart.getUser()) {
            throw new UnauthorizedException();
        }

        this.shoppingCartDAO.delete(shoppingCart);
    }
}
