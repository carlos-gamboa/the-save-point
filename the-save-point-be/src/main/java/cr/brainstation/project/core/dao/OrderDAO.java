package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.OrderDTO;
import cr.brainstation.project.model.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDAO extends JpaRepository<OrderDTO, Long> {

    @Query("select distinct o from OrderDTO as o inner join o.shoppingCart as s inner join s.user as u where u.id = :userId and o.status = :status order by o.arrivingDate DESC")
    List<OrderDTO> findAllByUserAndStatus(@Param("userId") Long userId, @Param("status") OrderStatus status);

    List<OrderDTO> findAllByStatus(OrderStatus status);
}
