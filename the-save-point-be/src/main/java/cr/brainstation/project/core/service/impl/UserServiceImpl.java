package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.ShoppingCartDAO;
import cr.brainstation.project.core.dao.UserDAO;
import cr.brainstation.project.core.dto.UserDTO;
import cr.brainstation.project.core.service.UserService;
import cr.brainstation.project.model.Login;
import cr.brainstation.project.model.ShoppingCartItem;
import cr.brainstation.project.model.User;
import cr.brainstation.project.support.exception.EmailAlreadyInUseException;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FailedLoginException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.jwt.JWTHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private ShoppingCartDAO shoppingCartDAO;

    @Autowired
    private JWTHelper jwtHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl() {
    }

    @Override
    @Transactional
    public User createUser(User user) throws RuntimeException {
        if (user.getUsername() == null) {
            throw new FieldRequiredException("Username");
        }
        if (user.getPassword() == null) {
            throw new FieldRequiredException("Password");
        }
        if (user.getFirstName() == null) {
            throw new FieldRequiredException("First Name");
        }
        UserDTO newUser = this.userDAO.findByUsername(user.getUsername());
        if (newUser != null) {
            throw new EmailAlreadyInUseException();
        }

        newUser = new UserDTO();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(this.passwordEncoder.encode(user.getPassword()));
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setDateOfBirth(user.getDateOfBirth());
        newUser.setGender(user.getGender());
        newUser.setProfilePictureLink(user.getProfilePictureLink());

        newUser = this.userDAO.save(newUser);

        return new User(newUser);
    }

    @Override
    @Transactional
    public User getUser(String id) throws RuntimeException {
        UserDTO user = this.userDAO.findById(Long.parseLong(id)).orElse(null);
        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }
        User userToReturn = new User(user);
        userToReturn.setPassword(null);
        return userToReturn;
    }

    @Override
    @Transactional
    public Login login(User user) throws RuntimeException {
        if (user.getUsername() == null) {
            throw new FieldRequiredException("Username");
        }
        if (user.getPassword() == null) {
            throw new FieldRequiredException("Password");
        }
        UserDTO newUser = this.userDAO.findByUsername(user.getUsername());
        if (newUser == null) {
            throw new EntityNotFoundException("user", "email");
        }
        if(!this.passwordEncoder.matches(user.getPassword(), newUser.getPassword())) {
            throw new FailedLoginException();
        }
        Login login = new Login();
        login.setToken(this.jwtHelper.generateToken(new User(newUser)));
        login.setFirstName(newUser.getFirstName());
        login.setLastName(newUser.getLastName());
        login.setUserId(newUser.getId());
        login.setShoppingCart(this.shoppingCartDAO.findAllByUserAndOrderIsNull(newUser).stream()
                .map(ShoppingCartItem::new)
                .collect(Collectors.toList()));
        return login;
    }

    @Override
    @Transactional
    public Login renew(String id) throws RuntimeException {
        UserDTO newUser = this.userDAO.findById(Long.parseLong(id)).orElse(null);
        if (newUser == null) {
            throw new EntityNotFoundException("user", "id");
        }
        Login login = new Login();
        login.setToken(this.jwtHelper.generateToken(new User(newUser)));
        login.setFirstName(newUser.getFirstName());
        login.setLastName(newUser.getLastName());
        login.setUserId(newUser.getId());

        return login;
    }

}
