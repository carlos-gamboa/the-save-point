package cr.brainstation.project.core.dto.converter;

import cr.brainstation.project.model.enums.ProductCategory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ProductCategoryConverter implements AttributeConverter<ProductCategory, String> {

    @Override
    public String convertToDatabaseColumn(ProductCategory attribute) {
        switch (attribute) {
            case CONSOLE:
                return "console";
            case VIDEO_GAME:
                return "video_game";
            case ACCESSORY:
                return "accessory";
            default:
                return "";
        }
    }

    @Override
    public ProductCategory convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "console":
                return ProductCategory.CONSOLE;
            case "video_game":
                return ProductCategory.VIDEO_GAME;
            case "accessory":
                return ProductCategory.ACCESSORY;
            default:
                return ProductCategory.NONE;
        }
    }
}

