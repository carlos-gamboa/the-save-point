package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Review;

public interface ReviewService {

    Review addReview(String userId, Review review) throws RuntimeException;

    Review updateReview(String userId, String reviewId, Review review) throws RuntimeException;

    Review getReview(String reviewId) throws RuntimeException;

    void deleteReview(String reviewId) throws RuntimeException;

}
