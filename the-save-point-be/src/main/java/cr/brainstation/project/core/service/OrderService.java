package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Order;

import java.util.List;

public interface OrderService {

    List<Order> getOrders(String userId) throws RuntimeException;

    Order createOrder(String userId, Order order) throws RuntimeException;

    Order updateOrder(String userId, String orderId, Order order) throws RuntimeException;

    Order getOrder(Long orderId);

}
