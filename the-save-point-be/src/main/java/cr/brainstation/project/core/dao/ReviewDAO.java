package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.ReviewDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewDAO extends JpaRepository<ReviewDTO, Long> {
}
