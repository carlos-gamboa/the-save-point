package cr.brainstation.project.core.dto;

import cr.brainstation.project.core.dto.converter.GenderConverter;
import cr.brainstation.project.model.enums.Gender;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Class that represents a person within the application.
 *
 */
@MappedSuperclass
public class PersonDTO extends BaseEntity {

    /**
     * First Name.
     */
    @Column(name = "first_name", nullable = false)
    protected String firstName;

    /**
     * Last Name.
     */
    @Column(name = "last_name")
    protected String lastName;

    /**
     * Date of birth.
     */
    @Column(name = "date_birth", nullable = false)
    protected LocalDate dateOfBirth;

    /**
     * Gender.
     */
    @Column(name = "gender", nullable = false)
    @Convert(converter = GenderConverter.class)
    protected Gender gender;

    /**
     * Link to the user's profile picture.
     */
    @Column(name = "profile_picture_link")
    protected String profilePictureLink;

    public PersonDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getProfilePictureLink() {
        return profilePictureLink;
    }

    public void setProfilePictureLink(String profilePictureLink) {
        this.profilePictureLink = profilePictureLink;
    }
}
