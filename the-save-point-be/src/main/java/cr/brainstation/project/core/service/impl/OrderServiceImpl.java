package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.OrderDAO;
import cr.brainstation.project.core.dao.ProductDAO;
import cr.brainstation.project.core.dao.ShoppingCartDAO;
import cr.brainstation.project.core.dao.UserDAO;
import cr.brainstation.project.core.dto.OrderDTO;
import cr.brainstation.project.core.dto.ProductDTO;
import cr.brainstation.project.core.dto.ShoppingCartItemDTO;
import cr.brainstation.project.core.dto.UserDTO;
import cr.brainstation.project.core.service.OrderService;
import cr.brainstation.project.model.Order;
import cr.brainstation.project.model.ShoppingCartItem;
import cr.brainstation.project.model.enums.OrderStatus;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.NotEnoughProductException;
import cr.brainstation.project.support.exception.UnauthorizedException;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ShoppingCartDAO shoppingCartDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private UserDAO userDAO;;

    @Override
    @Transactional
    public Order createOrder(String userId, Order order) {
        UserDTO user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }
        if (user.getId() != Long.parseLong(userId)) {
            throw new UnauthorizedException();
        }

        OrderDTO orderDTO = new OrderDTO(order);

        Set<ShoppingCartItemDTO> shoppingCart = new HashSet<>();

        for (ShoppingCartItem shoppingCartItem : order.getShoppingCart()) {
            ShoppingCartItemDTO shoppingCartItemDTO = this.shoppingCartDAO.findById(shoppingCartItem.getId()).orElse(null);
            if (shoppingCartItemDTO == null) {
                throw new EntityNotFoundException("shopping cart", "id");
            }

            shoppingCartItemDTO.setOrder(orderDTO);
            shoppingCartItemDTO = this.shoppingCartDAO.save(shoppingCartItemDTO);
            shoppingCart.add(shoppingCartItemDTO);

            ProductDTO productDTO = shoppingCartItemDTO.getProduct();
            if (productDTO.getLeftInStock() - shoppingCartItemDTO.getQuantity() < 0) {
                throw new NotEnoughProductException(productDTO.getName());
            }
            productDTO.setLeftInStock(productDTO.getLeftInStock() - shoppingCartItemDTO.getQuantity());
            this.productDAO.save(productDTO);
        }

        orderDTO.setShoppingCart(shoppingCart);
        orderDTO = this.orderDAO.saveAndFlush(orderDTO);
        return new Order(orderDTO);
    }

    @Override
    @Transactional
    public Order getOrder(Long orderId) {
        OrderDTO orderDTO = this.orderDAO.findById(orderId).orElse(null);
        Hibernate.initialize(orderDTO.getBilling());
        return new Order(orderDTO);
    }

    @Override
    public List<Order> getOrders(String userId) throws RuntimeException {
        UserDTO user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        System.out.println(userId);

        return this.orderDAO.findAllByUserAndStatus(Long.parseLong(userId), OrderStatus.ACCEPTED).stream()
                .map(Order::new)
                .collect(Collectors.toList());
    }

    @Override
    public Order updateOrder(String userId, String orderId, Order order) throws RuntimeException {

        OrderDTO newOrder = this.orderDAO.findById(Long.parseLong(orderId)).orElse(null);

        if (newOrder == null) {
            throw new EntityNotFoundException("order", "id");
        }

        newOrder.setStatus(order.getStatus());
        newOrder = this.orderDAO.save(newOrder);

        return new Order(newOrder);
    }


}
