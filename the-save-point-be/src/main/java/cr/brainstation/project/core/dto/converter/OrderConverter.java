package cr.brainstation.project.core.dto.converter;

import cr.brainstation.project.model.enums.OrderStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class OrderConverter implements AttributeConverter<OrderStatus, String> {

    @Override
    public String convertToDatabaseColumn(OrderStatus attribute) {
        switch (attribute) {
            case PENDING:
                return "pending";
            case CANCELLED:
                return "cancelled";
            default:
                return "accepted";
        }
    }

    @Override
    public OrderStatus convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "pending":
                return OrderStatus.PENDING;
            case "cancelled":
                return OrderStatus.CANCELLED;
            default:
                return OrderStatus.ACCEPTED;
        }
    }

}
