package cr.brainstation.project.core.dto;

import javax.persistence.*;

/**
 * Class that represents an Accessory in the application.
 */
@Entity
@Table(name = "accessories")
@PrimaryKeyJoinColumn(name = "productId")
public class AccessoryDTO extends ProductDTO {

    /**
     * Set of the consoles that the Accessory works.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="console_id")
    private ConsoleDTO console;

    public AccessoryDTO() {
    }

    public ConsoleDTO getConsole() {
        return console;
    }

    public void setConsole(ConsoleDTO console) {
        this.console = console;
    }
}
