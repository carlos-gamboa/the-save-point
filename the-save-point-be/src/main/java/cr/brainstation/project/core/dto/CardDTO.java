package cr.brainstation.project.core.dto;

import cr.brainstation.project.model.Card;
import cr.brainstation.project.model.User;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "cards")
public class CardDTO extends BaseEntity {

    /**
     * Card's number.
     */
    @Column(name = "card_number", nullable = false)
    private String cardNumber;

    /**
     * Expiration Date of the Card.
     */
    @Column(name = "expiration_date", nullable = false)
    private String expirationDate;

    /**
     * Name of the Card's owner.
     */
    @Column(name = "card_name", nullable = false)
    private String cardName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable=false)
    private UserDTO user;

    public CardDTO() {
    }

    public CardDTO(Card card, User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());

        this.id = card.getId();
        this.cardName = card.getCardName();
        this.cardNumber = card.getCardNumber();
        this.expirationDate = card.getExpirationDate();
        this.setUser(userDTO);
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

}
