package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.OrderDAO;
import cr.brainstation.project.core.dao.ProductDAO;
import cr.brainstation.project.core.dao.ShoppingCartDAO;
import cr.brainstation.project.core.dto.OrderDTO;
import cr.brainstation.project.core.dto.ProductDTO;
import cr.brainstation.project.core.dto.ShoppingCartItemDTO;
import cr.brainstation.project.core.service.JobService;
import cr.brainstation.project.model.enums.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ShoppingCartDAO shoppingCartDAO;

    @Override
    public void checkOrderStatus() {
        List<OrderDTO> orders = this.orderDAO.findAllByStatus(OrderStatus.PENDING);
        for (OrderDTO order : orders) {
            if (order.getCreateDateTime().plusSeconds(60).isBefore(LocalDateTime.now())) {
                List<ShoppingCartItemDTO> shoppingCart = this.shoppingCartDAO.findAllByOrder(order);
                for (ShoppingCartItemDTO shoppingCartItem : shoppingCart) {
                    ProductDTO product = shoppingCartItem.getProduct();
                    product.setLeftInStock(product.getLeftInStock() + shoppingCartItem.getQuantity());
                    this.productDAO.save(product);

                    shoppingCartItem.setOrder(null);
                    this.shoppingCartDAO.save(shoppingCartItem);
                }
                order.setStatus(OrderStatus.CANCELLED);
                this.orderDAO.save(order);
            }
        }
    }

}
