package cr.brainstation.project.config;

import cr.brainstation.project.support.filter.JWTFilter;
import cr.brainstation.project.support.job.OrderJob;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;


@Configuration
@ComponentScan("cr.brainstation.project")
public class AppConfig {

    private static Logger log = LogManager.getLogger(AppConfig.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

    @Bean
    public FilterRegistrationBean <JWTFilter> filterRegistrationBean() {
        FilterRegistrationBean<JWTFilter> registrationBean = new FilterRegistrationBean<>();
        JWTFilter JWTFilter = new JWTFilter();

        registrationBean.setFilter(JWTFilter);
        registrationBean.addUrlPatterns("/user/renew/*");
        registrationBean.addUrlPatterns("/billing/*");
        registrationBean.addUrlPatterns("/card/*");
        registrationBean.addUrlPatterns("/shopping-cart/*");
        registrationBean.addUrlPatterns("/order/*");
        registrationBean.addUrlPatterns("/review/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JobDetailFactoryBean jobDetail() {
        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(OrderJob.class);
        jobDetailFactory.setDescription("Invoke Order Job service...");
        jobDetailFactory.setDurability(true);
        return jobDetailFactory;
    }

    @Bean
    public Trigger trigger(JobDetail job) {
        return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity("Qrtz_Trigger")
                .withDescription("Order trigger")
                .withSchedule(simpleSchedule().repeatForever().withIntervalInSeconds(30))
                .build();
    }

    @Bean
    public Scheduler scheduler(Trigger trigger, JobDetail job) {
        StdSchedulerFactory factory = new StdSchedulerFactory();
        Scheduler scheduler = null;
        try {
            factory.initialize(new ClassPathResource("quartz.properties").getInputStream());

            scheduler = factory.getScheduler();
            scheduler.setJobFactory(springBeanJobFactory());
            scheduler.scheduleJob(job, trigger);

            scheduler.start();
        } catch (Exception e) {
            log.error("On Scheduler: ", e);
        }
        return scheduler;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }


}
