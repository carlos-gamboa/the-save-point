package cr.brainstation.project.support.filter;

import cr.brainstation.project.support.jwt.JWTHelper;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(1)
public class JWTFilter implements Filter {

    private static Logger log = LogManager.getLogger(JWTFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        JWTHelper helper = new JWTHelper();
        String token = request.getHeader("auth-token");
        try {
            Claims claims = helper.parseJWT(token);
            request.setAttribute("userId", claims.getSubject());
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            log.info(e.getMessage());
            response.reset();
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }

    }
}