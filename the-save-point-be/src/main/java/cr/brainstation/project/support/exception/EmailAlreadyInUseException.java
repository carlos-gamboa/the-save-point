package cr.brainstation.project.support.exception;

public class EmailAlreadyInUseException extends RuntimeException {

    public EmailAlreadyInUseException() {
        super("There is already an user with that email.");
    }

}
