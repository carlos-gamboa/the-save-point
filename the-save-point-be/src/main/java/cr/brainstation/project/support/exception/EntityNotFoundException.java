package cr.brainstation.project.support.exception;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String entity, String field) {
        super("Couldn't find a " + entity + " with that " + field + ".");
    }
}
