package cr.brainstation.project.support.exception;

public class FieldRequiredException extends RuntimeException {

    public FieldRequiredException() {
    }

    public FieldRequiredException(String field) {
        super(field + " is required.");
    }
}
