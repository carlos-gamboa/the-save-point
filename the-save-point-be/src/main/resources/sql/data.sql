INSERT INTO product('product_name', 'price', 'brand', 'left_stock', 'release_date',
 'features', 'product_picture_link', 'category') VALUES ('Nintendo - Switch 32GB Console - Gray Joy-Con',
 '299.99', 'nintendo', 10, '03/03/2017',
 'Nintendo Switch console
Includes a Switch console, Switch dock, Joy-Con (L) and Joy-Con (R), 2 Joy-Con strap accessories, 1 Joy-Con grip, AC adapter, HDMI cable.

Home gaming system
At home the main unit rests in the Nintendo Switch dock, which connects the system to the TV and lets you play with family and friends in the comfort of your living room.

On-the-Go
Lift Nintendo Switch from the dock and instantly transition to handheld mode for on-the-go gaming. By sharing Joy-Con, players can go head-to-head while away from home.

32GB of internal storage
A portion of internal memory is reserved for use by the system. You can expand the capacity of Nintendo Switch by using microSDXC or microSDHC memory cards.

Tegra processor powered by NVIDIA
The powerful processor delivers stunning graphics performance.

802.11ac wireless connection
Nintendo Switch can connect to the Internet wirelessly. While docked, Nintendo Switch can also use a wired Internet connection via a LAN adapter.

Capture button
The Capture button is located on Joy-Con (L) and is used to take gameplay screenshots. The player can then view, edit (add text of various sizes, colors and positions) and post them to popular social media networks.

Motion Control
Each Joy-Con includes a gyroscope and accelerometer.

HD Rumble
Both Joy-Con controllers include advanced HD Rumble, which can provide compatible games with subtle vibrations that are much more realistic than before.

IR Motion Camera
Joy-Con (R) includes an IR Motion Camera that can detect the distance, shape and motion of nearby objects in specially designed games.

Enhanced Parental Control
The system includes various on-device parental controls, but parents can also download a smart device application that can be used to easily and conveniently monitor and set parental controls for Nintendo Switch.

Joy-Con controllers
They can be inserted into a Joy-Con grip accessory to be used like traditional controllers, held comfortably in each hand for independent left and right motion controls, or shared between two players for multiplayer gaming.

Surround sound
TV Mode supports 5.1 channel surround sound. Through the system''s built-in audio jack, stereo speakers or headphones can be used when playing in handheld or tabletop modes.','https://s3.amazonaws.com/brainstation-project-3/products/switch-black.jpg', 'console');
